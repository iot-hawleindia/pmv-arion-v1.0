################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
CMD_SRCS += \
../lnk_msp430f5419a.cmd 

C_SRCS += \
../Menu.c \
../RTC.c \
../display.c \
../gpio.c \
../i2c.c \
../main.c \
../timer.c \
../wdt.c 

C_DEPS += \
./Menu.d \
./RTC.d \
./display.d \
./gpio.d \
./i2c.d \
./main.d \
./timer.d \
./wdt.d 

OBJS += \
./Menu.obj \
./RTC.obj \
./display.obj \
./gpio.obj \
./i2c.obj \
./main.obj \
./timer.obj \
./wdt.obj 

OBJS__QUOTED += \
"Menu.obj" \
"RTC.obj" \
"display.obj" \
"gpio.obj" \
"i2c.obj" \
"main.obj" \
"timer.obj" \
"wdt.obj" 

C_DEPS__QUOTED += \
"Menu.d" \
"RTC.d" \
"display.d" \
"gpio.d" \
"i2c.d" \
"main.d" \
"timer.d" \
"wdt.d" 

C_SRCS__QUOTED += \
"../Menu.c" \
"../RTC.c" \
"../display.c" \
"../gpio.c" \
"../i2c.c" \
"../main.c" \
"../timer.c" \
"../wdt.c" 


