/*
 * Menu.c
 *  Modified on : Feb 05,2020
 *  Created on  : Oct 21, 2020
 *      Author  : Arjun Shankar
 */
#include "Menu.h"
char tempbuf[30];
extern struct alarm alarm[4];
struct alarm temp;
struct alarm manual={0,0,0,3,250};
struct alarm relay_op;
extern struct Infodata Info;
static Calendar timetoset = {0};
extern int Input;
extern int exit_flag;
extern void local_to_info(void);
void startup_message(void)
{
    /*clear display to show new content*/
    //display_clearDisplay();

    display_clearDisplay();

    Print_center(2, "WELCOME");
    Print_center(4, "MIPL");

    /*delay to show the content*/

    respWaitTime(2);
    while(checkTimerIsExpired() == TIMER_NOT_EXPIRED);

    display_clearDisplay();

    Print_center(1, "CONTROL");
    Print_center(3, "VALVE");
    Print_center(5, "ARION");

    respWaitTime(2);
    while(checkTimerIsExpired() == TIMER_NOT_EXPIRED);

    display_clearDisplay();

    /*print version details*/
    sprintf(tempbuf,"HW_VER: ");
    strcat(tempbuf,HW_VER);
    Print_center(2,tempbuf);
    sprintf(tempbuf,"SW_VER: ");
    strcat(tempbuf,SW_VER);
    Print_center(4,tempbuf);

    respWaitTime(2);
    while(checkTimerIsExpired() == TIMER_NOT_EXPIRED);

    display_clearDisplay();

}

void menu(int arrow_pos)
{
    Print_center(0,"MENU");
    clear_arrow();

    display_printText(CURSOR_X,arrow_pos," >");
    display_printText(MENU_X,1,"RTC");
    display_printText(MENU_X,2,"ALARM");
    display_printText(MENU_X,3,"CONTROL");
    display_printText(MENU_X,4,"FAILSAFE");
    display_printText(MENU_X,5,"EXIT");

    respWaitTime(30);
    readinput();
    if(Input == ENTER )
    {
        display_clearDisplay();
        switch(arrow_pos)
        {
        case 1: rtc_menu(1);
                break;
        case 2: alarm_menu(1);
                break;
        case 3: Manual_control(1);
                break;
        case 4:set_failsafe(1,0);
                break;
        case 5: exit_flag = 1;
                homescreen();
               break;
        default: break;
        }
    }
    else if(Input == UP)
    {
        arrow_pos--;
        if (arrow_pos < 1)
        {
            arrow_pos = 5;
        }
        menu(arrow_pos);
    }
    else if(Input == DOWN)
    {
        arrow_pos++;
        if (arrow_pos > 5)
        {
            arrow_pos = 1;
        }
        menu(arrow_pos);
    }
}

void set_failsafe(int arrow_pos,int set)
{

    clear_arrow();

    if(set == 1)
    {
        Print_center(0, "FAIL SAFE");
        display_printText(CURSOR_X,arrow_pos,">>");
    }
    else
    {   display_clearDisplay();
        Print_center(0, "FAIL SAFE");
        display_printText(CURSOR_X,arrow_pos," >");
        display_printText(MENU_X,2,"EXIT");
    }
    display_printText(MENU_X,1,"OPTION");
    if(Info.restart_act == 0)
    {
        display_printText(PARAMETER_X,1,"BACKUP");
    }
    else if(Info.restart_act == 1)
    {
        //display_printText(PARAMETER_X,1,"      ");
        display_printText(PARAMETER_X,1,"NEW   ");
    }
    respWaitTime(30);
    readinput();

    if (set == 0)
    {
        if(Input == ENTER )
        {
            display_clearDisplay();
            if(arrow_pos == 1)
            {
                set_failsafe(1,1);
            }
            else if(arrow_pos == 2)
            {
                menu(4);
            }
        }
        else if(Input == UP)
        {

            arrow_pos--;
            if (arrow_pos < 1)
            {
                arrow_pos = 2;
            }
            set_failsafe(arrow_pos,0);
        }
            else if(Input == DOWN)
            {

                arrow_pos++;
                if (arrow_pos > 2)
                {
                    arrow_pos = 1;
                }
                set_failsafe(arrow_pos,0);
            }
    }
    else if(set == 1)
    {
        if(Input == ENTER )
        {
            display_clearDisplay();
            if(arrow_pos == 1)
            {
                set_failsafe(1,0);
            }
        }
        else if(Input == UP||Input == DOWN)
        {

            if (Info.restart_act == 0)
            {
                Info.restart_act = 1;
            }
            else if (Info.restart_act == 1)
            {
                Info.restart_act = 0;
                Info.restart_flag = 0;
            }
            set_failsafe(arrow_pos,1);
        }
    }


}
void rtc_menu(int arrow_pos)
{
    Print_center(0, "RTC MENU");
    clear_arrow();

    display_printText(CURSOR_X,arrow_pos," >");
    display_printText(MENU_X,1,"SET DATE");
    display_printText(MENU_X,2,"SET TIME");
    display_printText(MENU_X,3,"EXIT");

    respWaitTime(30);
    readinput();
    if(Input == ENTER )
    {
        display_clearDisplay();
        if(arrow_pos == 1)
        {
            get_rtc(&timetoset);
            set_date(1);
        }
        else if (arrow_pos == 2)
        {
            get_rtc(&timetoset);
            set_time(1);
        }
        else if (arrow_pos == 3)
        {

            menu(1);
        }
    }
    else if(Input == UP)
    {

        arrow_pos--;
        if (arrow_pos < 1)
        {
            arrow_pos = 3;
        }
        rtc_menu(arrow_pos);
    }
    else if(Input == DOWN)
    {

        arrow_pos++;
        if (arrow_pos > 3)
        {
            arrow_pos = 1;
        }
        rtc_menu(arrow_pos);
    }
}

void alarm_menu(int arrow_pos)
{
    Print_center(0, "ALARM MENU");
    clear_arrow();
    display_printText(0,arrow_pos," >");
    display_printText(MENU_X,1,"CONFIGURE");
    display_printText(MENU_X,2,"VIEW");
    display_printText(MENU_X,3,"EXIT");
    respWaitTime(30);
    readinput();
    if(Input == ENTER )
    {
        display_clearDisplay();
        if(arrow_pos == 1)
        {
            configure_alarm(2);
        }
        else if (arrow_pos == 2)
        {
            view_alarm(2);
        }
        else if (arrow_pos == 3)
        {
            menu(2);
        }
    }
    else if(Input == UP)
    {

        arrow_pos--;
        if (arrow_pos < 1)
        {
            arrow_pos = 3;
        }
        alarm_menu(arrow_pos);
    }
    else if(Input == DOWN)
    {

        arrow_pos++;

        if (arrow_pos > 3)
        {
            arrow_pos = 1;
        }

        alarm_menu(arrow_pos);
    }
}
void relay_operation(struct alarm relay)
{


    if((Info.restart_act == 1)&&(Info.restart_flag == 1))
    {
        Print_center(2,"Please");
        Print_center(3,"Update");
        Print_center(4,"Time");
        __delay_cycles(500000);
        __delay_cycles(500000);
        __delay_cycles(500000);
        __delay_cycles(500000);
        display_clearDisplay();
        GPIO_setOutputLowOnPin(PORT_ALARM_LED,ALARM_LED1+ALARM_LED2+ALARM_LED3+ALARM_LED4);
    }
    else
    {

        if (relay.Valvestate == OFF)
        {
           /* switch(relay.relay)
            {
            case 1:GPIO_setOutputLowOnPin(PORT_RELAY,PIN_RELAY1);
                    break;
            case 2:GPIO_setOutputLowOnPin(PORT_RELAY,PIN_RELAY2);
                    break;
            case 3:GPIO_setOutputLowOnPin(PORT_RELAY,PIN_RELAY3);
                    break;
            case 4:GPIO_setOutputLowOnPin(PORT_RELAY,PIN_RELAY4);
                    break;
            default:break;
            }*/
        }
        else if(relay.Valvestate == ON)
        {
            /*switch(relay.relay)
            {
            case 1:GPIO_setOutputHighOnPin(PORT_RELAY,PIN_RELAY1);
                    break;
            case 2:GPIO_setOutputHighOnPin(PORT_RELAY,PIN_RELAY2);
                    break;
            case 3:GPIO_setOutputHighOnPin(PORT_RELAY,PIN_RELAY3);
                    break;
            case 4:GPIO_setOutputHighOnPin(PORT_RELAY,PIN_RELAY4);
                    break;
            default:break;
            }*/
        }
        else if(relay.Valvestate == DELAY)
        {
            local_to_info();
            timerInit_delay();

            switch(relay.relay)
            {
            case 1: GPIO_setOutputLowOnPin(PORT_RELAY,PIN_RELAY1);
                    __delay_cycles(500000);
                    GPIO_setOutputHighOnPin(PORT_RELAY,PIN_RELAY1);
                    respWaitTime_delay(relay.delay);
                    while(checkTimerIsExpired_delay() == TIMER_NOT_EXPIRED);
                    GPIO_setOutputLowOnPin(PORT_RELAY,PIN_RELAY1);
                    Print_center(2,"Valve Opened");
                    __delay_cycles(500000);
                    __delay_cycles(500000);
                    __delay_cycles(500000);
                    __delay_cycles(500000);
                    display_clearDisplay();

                    break;
            case 2: GPIO_setOutputLowOnPin(PORT_RELAY,PIN_RELAY2);
                    __delay_cycles(500000);
                    GPIO_setOutputHighOnPin(PORT_RELAY,PIN_RELAY2);
                    respWaitTime_delay(relay.delay);
                    while(checkTimerIsExpired_delay() == TIMER_NOT_EXPIRED);
                    GPIO_setOutputLowOnPin(PORT_RELAY,PIN_RELAY2);
                    Print_center(2,"Valve Closed");
                    __delay_cycles(500000);
                    __delay_cycles(500000);
                    __delay_cycles(500000);
                    __delay_cycles(500000);
                    display_clearDisplay();

                    break;
            case 3: /*GPIO_setOutputLowOnPin(PORT_RELAY,PIN_RELAY3);
                    __delay_cycles(500000);
                    GPIO_setOutputHighOnPin(PORT_RELAY,PIN_RELAY3);
                    respWaitTime_delay(relay.delay);
                    while(checkTimerIsExpired_delay() == TIMER_NOT_EXPIRED);
                    GPIO_setOutputLowOnPin(PORT_RELAY,PIN_RELAY3);*/
                    break;
            case 4: /*GPIO_setOutputLowOnPin(PORT_RELAY,PIN_RELAY4);
                    __delay_cycles(500000);
                    GPIO_setOutputHighOnPin(PORT_RELAY,PIN_RELAY4);
                    respWaitTime_delay(relay.delay);
                    while(checkTimerIsExpired_delay() == TIMER_NOT_EXPIRED);
                    GPIO_setOutputLowOnPin(PORT_RELAY,PIN_RELAY4);*/
                    break;
            default:
                   Print_center(2,"No Operation");
                   __delay_cycles(500000);
                   __delay_cycles(500000);
                   __delay_cycles(500000);
                   __delay_cycles(500000);
                   display_clearDisplay();
                   break;
            }
        }

        /*display_clearDisplay();
        Print_center(2,"operation");
        Print_center(3,"executed");
        respWaitTime(2);
        while(checkTimerIsExpired() == TIMER_NOT_EXPIRED);
        display_clearDisplay();*/
        GPIO_setOutputLowOnPin(PORT_ALARM_LED,ALARM_LED1+ALARM_LED2+ALARM_LED3+ALARM_LED4);
    }

}
void Manual_control(int arrow_pos)
{
    static char relay[5]={0},delay[5]={0};

    Print_center(0, "CONTROL");
    clear_arrow();

    display_printText(0,arrow_pos," >");

    display_printText(MENU_X,1,"VALVE");
    if(manual.relay == 1)
    {
        sprintf(relay,"%s","Open ");

    }
    else if (manual.relay == 2)
    {
        sprintf(relay,"%s","Close");
    }
    else
    {
        sprintf(relay,"%s","NOP  ");
    }
    display_printText(PARAMETER_X,1,relay);

    display_printText(MENU_X,2,"STATE");
    display_printText(PARAMETER_X,2,"     ");
    if (manual.Valvestate == ON)
    {
        display_printText(PARAMETER_X,2,"RES");
    }
    else if(manual.Valvestate == OFF)
    {
        display_printText(PARAMETER_X,2,"RES");
    }
    else if(manual.Valvestate == DELAY)
    {
        display_printText(PARAMETER_X,2,"DELAY");
    }

    display_printText(MENU_X,3,"DELAY");
    display_printText(PARAMETER_X,3,"     ");
    sprintf(delay,"%dms",manual.delay);
    display_printText(PARAMETER_X,3,delay);

    display_printText(MENU_X,4,"SET");

    display_printText(MENU_X,5,"EXIT");

    respWaitTime(30);
    readinput();
    if(Input == ENTER )
        {

            if(arrow_pos <=3 )
            {
                display_clearDisplay();
                change_manual(arrow_pos);
            }

            else if (arrow_pos == 4)
            {
                display_clearDisplay();
                relay_operation(manual);

                display_clearDisplay();
                menu(3);
            }
            else if (arrow_pos == 5)
            {
                display_clearDisplay();
                display_printText(8,2,"No Operation");
                respWaitTime(2);
                while(checkTimerIsExpired() == TIMER_NOT_EXPIRED);
                display_clearDisplay();
                menu(3);
            }
        }
        else if(Input == UP)
        {

            arrow_pos--;
            if (arrow_pos < 1)
            {
                arrow_pos = 5;
            }
            Manual_control(arrow_pos);
        }
        else if(Input == DOWN)
        {

            arrow_pos++;
            if (arrow_pos > 5)
            {
                arrow_pos = 1;
            }
            Manual_control(arrow_pos);
        }
}

void change_manual(int arrow_pos)
{
    static char relay[5]={0},delay[5]={0};

    Print_center(0, "CONTROL");
    clear_arrow();

    display_printText(0,arrow_pos,">>");

    display_printText(MENU_X,1,"VALVE");
    if(manual.relay == 1)
    {
        sprintf(relay,"%s","Open ");
    }
    else if (manual.relay == 2)
    {
        sprintf(relay,"%s","Close");
    }
    else
    {
        sprintf(relay,"%s","NOP  "); //No Operation   //"NOP  "
    }
    display_printText(PARAMETER_X,1,relay);

    display_printText(MENU_X,2,"STATE");
    display_printText(PARAMETER_X,2,"     ");
    if (manual.Valvestate == ON)
    {
        display_printText(PARAMETER_X,2,"RES");
    }
    else if(manual.Valvestate == OFF)
    {
        display_printText(PARAMETER_X,2,"RES");
    }
    else if(manual.Valvestate == DELAY)
    {
        display_printText(PARAMETER_X,2,"DELAY");
    }

    display_printText(MENU_X,3,"DELAY");
    display_printText(PARAMETER_X,3,"     ");
    sprintf(delay,"%dms",manual.delay);
    display_printText(PARAMETER_X,3,delay);

    respWaitTime(30);
    readinput();

    if(Input == ENTER )
    {

        if(arrow_pos <= 3 )
        {
            Manual_control(arrow_pos);
        }
    }
    else if(Input == UP)
    {
        if (arrow_pos == 1)
        {
            manual.relay++;
            if(manual.relay >= 3)
            {
                manual.relay = 0;
            }
        }
        if(arrow_pos == 2)
        {
            manual.Valvestate = 3;
        }
        if(arrow_pos == 3)
        {
            if(manual.Valvestate == DELAY)
            {
                manual.delay = manual.delay+5;
                if(manual.delay > 250)
                {
                    manual.delay = 50;
                }
            }
        }
        change_manual(arrow_pos);
    }
    else if(Input == DOWN)
    {

        if (arrow_pos == 1)
        {

            if(manual.relay == 0)
            {
                manual.relay = 3;
            }
            manual.relay--;
        }
        if(arrow_pos == 2)
        {
                manual.Valvestate = 3;
        }
        if(arrow_pos == 3)
        {
            if(manual.Valvestate == DELAY)
            {
                manual.delay = manual.delay-5;
                if(manual.delay <50)
                {
                    manual.delay = 250;
                }
            }
        }
        change_manual(arrow_pos);
    }

}

void set_date(int pos)
{
    static char day[5]={0},month[5]={0},year[5]={0};

    Print_center(0, "SET DATE");
    clear_arrow();

    display_printText(CURSOR_X,pos," >");

    display_printText(MENU_X,1,"DAY");
    sprintf(day,"%d",timetoset.DayOfMonth);
    display_printText(PARAMETER_X,1,day);

    display_printText(MENU_X,2,"MONTH");
    sprintf(month,"%d",timetoset.Month);
    display_printText(PARAMETER_X,2,month);

    display_printText(MENU_X,3,"YEAR");
    sprintf(year,"%d",timetoset.Year);
    display_printText(PARAMETER_X,3,year);

    display_printText(MENU_X,4,"SET");

    display_printText(MENU_X,5,"EXIT");

    respWaitTime(30);
    readinput();
    if(Input == ENTER )
        {
            display_clearDisplay();
            if((pos == 1)||(pos == 2)||(pos == 3))
            {

                set_date_change(pos);
            }
            else if (pos == 4)
            {
                change_RTCtime(timetoset);
                if(Info.restart_act == 1)
                {
                    Info.restart_flag = 0;
                }
                Print_center(4, "DATE UPDATED");
                respWaitTime(2);
                while(checkTimerIsExpired() == TIMER_NOT_EXPIRED);
                display_clearDisplay();
                rtc_menu(1);
            }
            else if(pos == 5)
            {
                Print_center(4, "DATE NOT UPDATED");
                respWaitTime(2);
                while(checkTimerIsExpired() == TIMER_NOT_EXPIRED);
                display_clearDisplay();
                rtc_menu(1);
            }
        }
        else if(Input == UP)
        {
            pos--;
            if (pos < 1)
            {
                pos = 5;
            }
            set_date(pos);
        }
        else if(Input == DOWN)
        {

            pos++;
            if (pos > 5)
            {
                pos = 1;
            }
            set_date(pos);
        }

}

void set_date_change(int pos)
{
    static char day[5]={0},month[5]={0},year[5]={0};


    Print_center(0, "SET DATE");
    clear_arrow();

    display_printText(CURSOR_X,pos,">>");

    display_printText(MENU_X,1,"DAY");
    sprintf(day,"%d",timetoset.DayOfMonth);
    display_printText(PARAMETER_X,1,"   ");
    display_printText(PARAMETER_X,1,day);

    display_printText(MENU_X,2,"MONTH");
    sprintf(month,"%d",timetoset.Month);
    display_printText(PARAMETER_X,2,"   ");
    display_printText(PARAMETER_X,2,month);

    display_printText(MENU_X,3,"YEAR");
    display_printText(PARAMETER_X,3,"   ");
    sprintf(year,"%d",timetoset.Year);
    display_printText(PARAMETER_X,3,year);

    respWaitTime(30);
    readinput();
    if(Input == ENTER )
    {
        display_clearDisplay();
        if((pos == 1)||(pos == 2)||(pos == 3))
        {
            set_date(pos);
        }

    }
    else if(Input == UP)
    {

        if(pos == 1)
        {
            timetoset.DayOfMonth++;
            check_date_overflow();

        }
        else if (pos == 2)
        {
            timetoset.Month++;
            if(timetoset.Month > 12)
            {
                timetoset.Month = 1;
            }
        }
        else if (pos == 3)
        {
            timetoset.Year++;
            if(timetoset.Year > 2099)
            {
                timetoset.Year = 2020;
            }
        }
        check_date_overflow(); // this function called for example user set Dec -31 and going to change month DEC to NOV date will automtically change to 1
        set_date_change(pos);
    }
    else if(Input == DOWN)
    {

        if(pos == 1)
        {
            timetoset.DayOfMonth--;
            check_date_underflow();
        }
        if (pos == 2)
        {
            timetoset.Month--;
            if(timetoset.Month < 1)
            {
                timetoset.Month = 12;
            }
        }
        if (pos == 3)
        {
            timetoset.Year--;
            if(timetoset.Year < 2020 )
            {
                timetoset.Year = 2020;
            }
        }
        check_date_overflow(); // this function called for example user set Dec -31 and going to change month DEC to NOV date will automtically change to 1
        set_date_change(pos);
    }
}

void set_time(int pos)
{
    static char hours[5]={0},mins[5]={0};

    Print_center(0, "SET TIME");
    clear_arrow();

    display_printText(CURSOR_X,pos," >");
    display_printText(MENU_X,1,"HOURS");
    sprintf(hours,"%d",timetoset.Hours);
    display_printText(PARAMETER_X,1,hours);

    display_printText(MENU_X,2,"MINUTES");
    sprintf(mins,"%d",timetoset.Minutes);
    display_printText(PARAMETER_X,2,mins);

    display_printText(MENU_X,3,"SET");

    display_printText(MENU_X,4,"EXIT");

    respWaitTime(30);
    readinput();
    if(Input == ENTER )
    {
        display_clearDisplay();
        if(pos == 1||pos == 2)
        {
            set_time_change(pos);
        }
        else if (pos == 3)
        {
            timetoset.Seconds = 0;      //clear the seconds to zero 24/3/21 sridhar
            change_RTCtime(timetoset);
            if(Info.restart_act == 1)
            {
                Info.restart_flag = 0;
            }
            set_next_alarm();
            Print_center(4, "TIME UPDATED");
            respWaitTime(2);
            while(checkTimerIsExpired() == TIMER_NOT_EXPIRED);
            display_clearDisplay();
            rtc_menu(2);
        }
        else if(pos == 4)
        {
            Print_center(4, "TIME NOT UPDATED");
            respWaitTime(2);
            while(checkTimerIsExpired() == TIMER_NOT_EXPIRED);
            display_clearDisplay();
            rtc_menu(2);
        }
    }
    else if(Input == UP)
    {
        pos--;
        if (pos < 1)
        {
            pos = 4;
        }
        set_time(pos);
    }
    else if(Input == DOWN)
    {

        pos++;
        if (pos > 4)
        {
            pos = 1;
        }
        set_time(pos);
    }
}

void set_time_change(int pos)
{
    static char hours[5]={0},mins[5]={0};

    Print_center(0, "SET TIME");

    clear_arrow();
    display_printText(CURSOR_X,pos,">>");

    display_printText(MENU_X,1,"HOURS");
    sprintf(hours,"%d",timetoset.Hours);
    display_printText(PARAMETER_X,1,"   ");
    display_printText(PARAMETER_X,1,hours);

    display_printText(MENU_X,2,"MINUTES");
    sprintf(mins,"%d",timetoset.Minutes);
    display_printText(PARAMETER_X,2,"   ");
    display_printText(PARAMETER_X,2,mins);

    respWaitTime(30);

    readinput();
    if(Input == ENTER )
    {
        display_clearDisplay();
        if(pos == 1||pos == 2)
        {
            set_time(pos);
        }
    }
    else if(Input == UP)
    {

        if(pos == 1)
        {
            timetoset.Hours++;
            if(timetoset.Hours > 23)
            {
                timetoset.Hours = 0;
            }
        }
        if (pos == 2)
        {
            timetoset.Minutes++;
            if(timetoset.Minutes > 59)
            {
                timetoset.Minutes=0;
            }
        }
        set_time_change(pos);
    }
    else if(Input == DOWN)
    {

        if(pos == 1)
        {

            if(timetoset.Hours == 0)
            {
                timetoset.Hours = 23;
            }
            else
            {
                timetoset.Hours--;
            }
        }
        if (pos == 2)
        {
            if(timetoset.Minutes == 0)
            {
                timetoset.Minutes = 59;
            }
            else
            {
                timetoset.Minutes--;
            }

        }
        set_time_change(pos);
    }
}


void view_alarm(int arrow_pos)
{
    Print_center(0, "VIEW ALARM");
    clear_arrow();

    display_printText(0,arrow_pos," >");
    display_printText(MENU_X,2,"ALARM1");
    display_printText(MENU_X,3,"ALARM2");
    display_printText(MENU_X,4,"ALARM3");
    display_printText(MENU_X,5,"ALARM4");
    display_printText(MENU_X,6,"EXIT");

    respWaitTime(30);
    readinput();
    if(Input == ENTER )
    {
        display_clearDisplay();
        if(arrow_pos <=5)
        {
            temp = alarm[arrow_pos-2];
            show_alarm(1,(arrow_pos-1));
        }
        else if (arrow_pos == 6)
        {
            display_clearDisplay();
            alarm_menu(2);
        }
    }
    else if(Input == UP)
    {

        arrow_pos--;
        if (arrow_pos < 2)
        {
            arrow_pos = 6;
        }

        view_alarm(arrow_pos);
    }
    else if(Input == DOWN)
    {

        arrow_pos++;

        if (arrow_pos > 6)
        {
            arrow_pos = 2;
        }
        view_alarm(arrow_pos);
    }
}

void show_alarm(unsigned int pos,int num)
{
    pos = 6;//to set cursor at exit
    static char alarm_hrs[5]={0},alarm_mins[5]={0},title[20]={0},relay[5]={0},delay[10]={0};
    sprintf(title,"ALARM%d",num);

    Print_center(0,title);
    clear_arrow();

    display_printText(0,pos," >");

    display_printText(MENU_X,1,"HOURS");
    if(temp.hours<10)
    {
        sprintf(alarm_hrs,"0%d",temp.hours);
    }
    else
    {
        sprintf(alarm_hrs,"%d",temp.hours);
    }
    display_printText(PARAMETER_X,1,alarm_hrs);

    display_printText(MENU_X,2,"MINUTES");
    if(temp.minutes<10)
    {
        sprintf(alarm_mins,"0%d",temp.minutes);
    }
    else
    {
        sprintf(alarm_mins,"%d",temp.minutes);
    }
    display_printText(PARAMETER_X,2,alarm_mins);

    display_printText(MENU_X,3,"VALVE");

    if(temp.relay == 1)
        {
         sprintf(relay,"Open ");
        }
    else if (temp.relay == 2)
         {
          sprintf(relay,"Close");
         }
     else
         {
            temp.relay = 0 ;
            sprintf(relay,"NOP  ");
         }

    display_printText(PARAMETER_X,3,relay);

    display_printText(MENU_X,4,"STATE");
    display_printText(PARAMETER_X,4,"     ");
    if (temp.Valvestate == ON)
    {
        display_printText(PARAMETER_X,4,"RES");
    }
    else if(temp.Valvestate == OFF)
    {
        display_printText(PARAMETER_X,4,"RES");
    }
    else if(temp.Valvestate == DELAY)
    {
        display_printText(PARAMETER_X,4,"DELAY");
    }
    display_printText(MENU_X,5,"DELAY");
    if(temp.delay<10)
    {
        sprintf(delay,"00%dms",temp.delay);
    }
    else if(temp.delay<100)
    {
        sprintf(delay,"0%dms",temp.delay);
    }
    else
    {
        sprintf(delay,"%dms",temp.delay);
    }
    display_printText(PARAMETER_X,5,delay);

    display_printText(MENU_X,6,"EXIT");

    respWaitTime(30);
    readinput();

    if(Input == ENTER )
    {
        display_clearDisplay();
        view_alarm(num+1);
    }
    else if(Input == UP)
    {
        show_alarm(1,num);
    }
    else if(Input == DOWN)
    {
        show_alarm(1,num);
    }
}

void configure_alarm(int arrow_pos)
{
    Print_center(0, "SET ALARM");
    clear_arrow();
    display_printText(0,arrow_pos," >");

    display_printText(MENU_X,2,"ALARM1");
    display_printText(MENU_X,3,"ALARM2");
    display_printText(MENU_X,4,"ALARM3");
    display_printText(MENU_X,5,"ALARM4");
    display_printText(MENU_X,6,"EXIT");

    respWaitTime(30);
    readinput();
    if(Input == ENTER )
    {
        display_clearDisplay();
        if(arrow_pos <= 5)
        {
            temp = alarm[arrow_pos-2];
            set_alarm(1,(arrow_pos-1));
        }
        else if (arrow_pos == 6)
        {
            alarm_menu(1);
        }
    }
    else if(Input == UP)
    {

        arrow_pos--;
        if (arrow_pos < 2)
        {
            arrow_pos = 6;
        }

        configure_alarm(arrow_pos);
    }
    else if(Input == DOWN)
    {

        arrow_pos++;

        if (arrow_pos > 6)
        {
            arrow_pos = 2;
        }
        configure_alarm(arrow_pos);
    }
}

void set_alarm(unsigned int pos,int num)
{
    static char alarm_hrs[5]={0},alarm_mins[5]={0},title[20]={0},relay[5]={0},delay[10]={0};

    sprintf(title,"SET ALARM%d",num);

    Print_center(0,title);
    clear_arrow();

    display_printText(0,pos," >");

    display_printText(MENU_X,1,"HOURS");
    if(temp.hours<10)
    {
        sprintf(alarm_hrs,"0%d",temp.hours);
    }
    else
    {
        sprintf(alarm_hrs,"%d",temp.hours);
    }
    display_printText(PARAMETER_X,1,alarm_hrs);

    display_printText(MENU_X,2,"MINUTES");
    if(temp.minutes<10)
    {
        sprintf(alarm_mins,"0%d",temp.minutes);
    }
    else
    {
        sprintf(alarm_mins,"%d",temp.minutes);
    }
    display_printText(PARAMETER_X,2,alarm_mins);

    display_printText(MENU_X,3,"VALVE");

    if(temp.relay == 1)
       {
         sprintf(relay,"Open ");
       }
   else if (temp.relay == 2)
        {
         sprintf(relay,"Close");
        }
    else
       {
         temp.relay = 0;
          sprintf(relay,"NOP  ");
       }
    display_printText(PARAMETER_X,3,relay);

    display_printText(MENU_X,4,"STATE");
    display_printText(PARAMETER_X,4,"     ");
    if (temp.Valvestate == ON)
    {
        display_printText(PARAMETER_X,4,"RES");
    }
    else if(temp.Valvestate == OFF)
    {
        display_printText(PARAMETER_X,4,"RES");
    }
    else if(temp.Valvestate == DELAY)
    {
        display_printText(PARAMETER_X,4,"DELAY");
    }

    display_printText(MENU_X,5,"DELAY");
    if(temp.delay<10)
    {
        sprintf(delay,"00%dms",temp.delay);
    }
    else if(temp.delay<100)
    {
        sprintf(delay,"0%dms",temp.delay);
    }
    else
    {
        sprintf(delay,"%dms",temp.delay);
    }
    display_printText(PARAMETER_X,5,delay);

    display_printText(MENU_X,6,"SET");

    display_printText(MENU_X,7,"EXIT");

    respWaitTime(30);
    readinput();

    if(Input == ENTER )
    {
        display_clearDisplay();
        if((pos == 1)|| (pos == 2) || (pos == 3) || (pos==4) || (pos==5))
        {
            set_alarm_change(pos,num);
        }
        else if (pos == 6)
        {
            alarm[num-1] = temp;
            set_next_alarm();            // 22/3/21 changed by sridhar to fix the UI working time alarm wakeup not happened
                                          // store the changed values in RTC register
            Print_center(4, "ALARM UPDATED");
            respWaitTime(2);
            while(checkTimerIsExpired() == TIMER_NOT_EXPIRED);
            display_clearDisplay();
            configure_alarm(num+1);

        }
        else if(pos == 7)
        {
            Print_center(4, "ALARM NOT UPDATED");
            respWaitTime(2);
            while(checkTimerIsExpired() == TIMER_NOT_EXPIRED);
            display_clearDisplay();
            configure_alarm(num+1);
        }
    }
    else if(Input == UP)
    {
        pos--;
        if (pos < 1)
        {
            pos = 7;
        }
        set_alarm(pos,num);
    }
    else if(Input == DOWN)
    {
        pos++;
        if (pos > 7)
        {
            pos = 1;
        }
        set_alarm(pos,num);
    }
}

void set_alarm_change(unsigned int pos,int num)
{
    static char alarm_hrs[5]={0},alarm_mins[5]={0},title[20]={0},relay[10]={0},delay[10]={0};

    sprintf(title,"SET ALARM%d",num);
    Print_center(0,title);
    clear_arrow();

    display_printText(0,pos,">>");

    display_printText(MENU_X,1,"HOURS");
    if(temp.hours<10)
    {
        sprintf(alarm_hrs,"0%d",temp.hours);
    }
    else
    {
        sprintf(alarm_hrs,"%d",temp.hours);
    }
    display_printText(PARAMETER_X,1,alarm_hrs);

    display_printText(MENU_X,2,"MINUTES");
    if(temp.minutes<10)
    {
        sprintf(alarm_mins,"0%d",temp.minutes);
    }
    else
    {
        sprintf(alarm_mins,"%d",temp.minutes);
    }
    display_printText(PARAMETER_X,2,alarm_mins);

    display_printText(MENU_X,3,"VALVE"); //RELAY sri


        if(temp.relay == 1)
        {
            sprintf(relay,"Open ");
        }
        else if (temp.relay == 2)
        {
            sprintf(relay,"Close");
        }
        else
        {
            temp.relay = 0 ;
            sprintf(relay,"NOP  ");
        }
      display_printText(PARAMETER_X,3,relay);

    display_printText(MENU_X,4,"STATE");
    display_printText(PARAMETER_X,4,"     ");
    if (temp.Valvestate == ON)
    {
        display_printText(PARAMETER_X,4,"RES");
    }
    else if(temp.Valvestate == OFF)
    {
        display_printText(PARAMETER_X,4,"RES");
    }
    else if(temp.Valvestate == DELAY)
    {
        display_printText(PARAMETER_X,4,"DELAY");
    }

    display_printText(MENU_X,5,"DELAY");
    if(temp.delay<10)
    {
        sprintf(delay,"00%dms",temp.delay);
    }
    else if(temp.delay<100)
    {
        sprintf(delay,"0%dms",temp.delay);
    }
    else
    {
        sprintf(delay,"%dms",temp.delay);
    }
    display_printText(PARAMETER_X,5,delay);

    respWaitTime(30);
    readinput();
    if(Input == ENTER )
    {

        if((pos == 1)|| (pos == 2) || (pos == 3) || (pos==4) || (pos==5))
        {
            set_alarm(pos,num);
        }
    }
    else if(Input == UP)
    {

        if(pos == 1)
        {
            temp.hours++;
            if(temp.hours > 23)
            {
                temp.hours=0;
            }
        }
        if (pos == 2)
        {
            temp.minutes++;
            if(temp.minutes > 59)
            {
                temp.minutes = 0;
            }
        }
//        if (pos == 3)
//        {
//            temp.relay++;
//            if(temp.relay >= 2)   // if(temp.relay > 4) sridhar
//            {
//                temp.relay = 0;
//            }
//        }

        if (pos == 3)
        {
            temp.relay++;
            if(temp.relay > 2)
            {
                temp.relay = 0;
            }
        }

        if(pos == 4)
        {
            temp.Valvestate = 3;
        }
        if(pos == 5)
        {
            if(temp.Valvestate == DELAY)
            {
                temp.delay = temp.delay + 5 ;
                if(temp.delay > 250)
                {
                    temp.delay = 50;
                }
            }
        }
        set_alarm_change(pos,num);
    }
    else if(Input == DOWN)
    {

        if(pos == 1)
        {

            if(temp.hours == 0)
            {
                temp.hours = 23;
            }
            else
            {
                temp.hours--;
            }
        }
        if (pos == 2)
        {

            if(temp.minutes == 0)
            {
                temp.minutes = 59;
            }
            else
            {
                temp.minutes--;
            }
        }
//        if (pos == 3)
//        {
//            temp.relay--;
//            if(temp.relay < 1)
//            {
//                temp.relay = 4;
//            }
//        }

        if (pos == 3)
        {
            temp.relay++;
            if(temp.relay > 2)
            {
                temp.relay = 0;
            }
        }

        if(pos == 4)
        {
                temp.Valvestate = 3;
        }
        if(pos == 5)
        {
            if(temp.Valvestate == DELAY)
            {
                temp.delay = temp.delay - 5;
                if(temp.delay <50)
                {
                    temp.delay = 250;
                }

            }
        }
        set_alarm_change(pos,num);
    }
}

void clear_arrow(void)
{
    int i=0;
    while(i<8)
    {
        display_printText(CURSOR_X,i,"  ");
        i++;
    }
}
void check_date_overflow(void)
{
    uint8_t leap_yr = 0;
    if(timetoset.Month == 2)
    {
        if((timetoset.Year % 400) == 0)
        {
            leap_yr = 1;
        }
        else if((timetoset.Year % 100) == 0)
        {
            leap_yr = 0;
        }
        else if((timetoset.Year % 4) == 0)
        {
            leap_yr = 1;
        }
        else
        {
            leap_yr = 0;
        }

        if((timetoset.DayOfMonth > 29) && leap_yr == 1)
        {
            timetoset.DayOfMonth = 1;
        }
        else if(timetoset.DayOfMonth > 28 && leap_yr == 0)
        {
            timetoset.DayOfMonth = 1;
        }
    }
    if((timetoset.DayOfMonth > 30) && (timetoset.Month == 4 || timetoset.Month == 6 || timetoset.Month == 9 || timetoset.Month == 11))
    {
        timetoset.DayOfMonth = 1;
    }

    else if(timetoset.DayOfMonth > 31)
    {
        timetoset.DayOfMonth = 1;
    }


}

void check_date_underflow(void)
{
    //for < 1 values
    uint8_t leap_yr = 0;
    if(timetoset.Month == 2) // check leaf year
      {
         if((timetoset.Year % 400) == 0)
           {
             leap_yr = 1;
            }
            else if((timetoset.Year % 100) == 0)
            {
                leap_yr = 0;
            }
            else if((timetoset.Year % 4) == 0)
            {
                leap_yr = 1;
            }
            else
            {
                leap_yr = 0;
            }

            if((timetoset.DayOfMonth < 1) && leap_yr == 1)
            {
                timetoset.DayOfMonth = 29;
            }
            else if(timetoset.DayOfMonth < 1 && leap_yr == 0)
            {
                timetoset.DayOfMonth = 28;
            }
    }

        if((timetoset.DayOfMonth < 1) && (timetoset.Month == 4 || timetoset.Month == 6 || timetoset.Month == 9 || timetoset.Month == 11))
        {
            timetoset.DayOfMonth = 30;
        }

        else if(timetoset.DayOfMonth < 1)
        {
            timetoset.DayOfMonth = 31;
        }
}
