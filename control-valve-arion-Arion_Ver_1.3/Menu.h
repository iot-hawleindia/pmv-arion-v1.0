/*
 * Menu.h
 *  Modified on : Feb 05,2020
 *  Created on  : Oct 21, 2020
 *      Author  : Arjun shankar
 */

#ifndef MENU_H_
#define MENU_H_

#include "config.h"

void startup_message(void);
void clear_arrow(void);

void menu(int arrow_pos);
void rtc_menu(int arrow_pos);
void alarm_menu(int arrow_pos);
void set_date(int pos);
void set_time(int pos);
void set_time_change(int pos);
void set_date_change(int pos);
void configure_alarm(int arrow_pos);
void set_alarm(unsigned int pos,int num);
void set_alarm_change(unsigned int pos,int num);
void show_alarm(unsigned int pos,int num);
void view_alarm(int arrow_pos);
void Manual_control(int arrow_pos);
void change_manual(int arrow_pos);
void relay_operation(struct alarm relay);
void set_failsafe(int arrow_pos,int set);
void check_date_overflow(void);
void check_date_underflow(void);





#endif /* MENU_H_ */
