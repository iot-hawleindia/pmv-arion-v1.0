/*
 * RTC.c
 *  Modified on : Feb 05,2020
 *  Created on  : Sep 25, 2020
 *      Author  : Arjun Shankar
 */
#include "config.h"

/*variable declaration*/
static int Alarm_flag = 0;                              //flag for alarm interrupt
static Calendar currentTime = {0};                             //structure for current time
extern struct alarm alarm[4];                           //structure for alarm configurations
RTC_A_configureCalendarAlarmParam alarmSettings = {0};  //structure for configuring alarm registers
extern char tempbuf[20];                                //Dummy buffer
extern struct Infodata Info;
void rtcinit(void)
{

    /*hold RTC module before configuring alarm registers*/
    RTC_A_holdClock(RTC_A_BASE);


    /*Default value copied to calendar parameters*/
    currentTime.Seconds     = DEFAULT_SEC;
    currentTime.Minutes     = DEFAULT_MIN;
    currentTime.Hours       = DEFAULT_HOUR;
    //currentTime.DayOfWeek   = DEFAULT_DOW;
    currentTime.DayOfMonth  = DEFAULT_DAY;
    currentTime.Month       = DEFAULT_MONTH;
    currentTime.Year        = DEFAULT_YEAR;

    /*RTC initialized in calendar mode with binary format*/
    RTC_A_initCalendar(RTC_A_BASE, &currentTime, RTC_A_FORMAT_BINARY);
    /*Set RTC calibration Value*/
    RTC_A_setCalibrationData(RTC_A_BASE,RTC_A_CALIBRATION_DOWN2PPM,23);
    /*start RTC clock*/
    RTC_A_startClock(RTC_A_BASE);
}

void get_rtc(Calendar *Time)
{
    *Time = RTC_A_getCalendarTime(RTC_A_BASE);
}

int check_alarmwakeup(void)
{
   return Alarm_flag;
}

void reset_alarmwakeup(void)
{
    Alarm_flag=0;
}

void set_next_alarm(void)
{
    int diff_H[4] = {0};
    get_rtc(&currentTime) ;
    unsigned int i =0,min;
    /* clear interrupts before assigning new values */
    RTC_A_clearInterrupt(RTC_A_BASE,RTCAIE);
    /* AIE bit disabled */
    alarmSettings.dayOfMonthAlarm = RTC_A_ALARMCONDITION_OFF;
    /* AIE bit disabled */
    alarmSettings.dayOfWeekAlarm = RTC_A_ALARMCONDITION_OFF;

    for(i=0;i<4;i++)
    {
        if(alarm[i].relay != 0)
        {
        if(currentTime.Hours < alarm[i].hours)
        {
           diff_H[i] = alarm[i].hours - currentTime.Hours;
        }
        if(currentTime.Hours > alarm[i].hours )
        {
            diff_H[i] = (24 - currentTime.Hours) + alarm[i].hours ;
        }
        if(currentTime.Hours == alarm[i].hours )
        {
            if(currentTime.Minutes < alarm[i].minutes)
            {
                diff_H[i] = alarm[i].hours - currentTime.Hours;
            }
            if(currentTime.Minutes >= alarm[i].minutes)
            {
                diff_H[i] = (24 - currentTime.Hours) + alarm[i].hours ;
            }

        }

        if(i == 0)
        {
            alarmSettings.hoursAlarm = alarm[i].hours;
            alarmSettings.minutesAlarm = alarm[i].minutes;
            min = i;
        }
        else
        {
            if(diff_H[min] > diff_H[i])
            {
                alarmSettings.hoursAlarm = alarm[i].hours;
                alarmSettings.minutesAlarm = alarm[i].minutes;
                min = i;
            }
            if(diff_H[min] == diff_H[i])
            {
                if(alarm[min].minutes > alarm[i].minutes)
                {
                    alarmSettings.minutesAlarm = alarm[i].minutes;
                    min = i;
                }
                else if (alarm[min].minutes < alarm[i].minutes)
                {
                    alarmSettings.minutesAlarm = alarm[min].minutes;
                }
            }
        }
    }
    }

    RTC_A_enableInterrupt(RTC_A_BASE,RTCAIE);
    RTC_A_configureCalendarAlarm(RTC_A_BASE, &alarmSettings);

    Info.current_alarm = min;

}

void change_RTCtime ( Calendar time2Save )
{
    RTC_A_holdClock(RTC_A_BASE);
    RTC_A_initCalendar(RTC_A_BASE, &time2Save, RTC_A_FORMAT_BINARY);
    RTC_A_startClock(RTC_A_BASE);
}

void print_alarm(void)
{
    notifyWDT();
    display_clearDisplay();
    Print_center(2, "next alarm");
    if((alarmSettings.hoursAlarm<10) && (alarmSettings.minutesAlarm < 10))
    {
        sprintf(tempbuf,"0%d:0%d",alarmSettings.hoursAlarm,alarmSettings.minutesAlarm);
    }
    else if(alarmSettings.hoursAlarm<10)
    {
        sprintf(tempbuf,"0%d:%d",alarmSettings.hoursAlarm,alarmSettings.minutesAlarm);

    }
    else if(alarmSettings.minutesAlarm < 10)
    {
        sprintf(tempbuf,"%d:0%d",alarmSettings.hoursAlarm,alarmSettings.minutesAlarm);

    }
    else
    {
        sprintf(tempbuf,"%d:%d",alarmSettings.hoursAlarm,alarmSettings.minutesAlarm);

    }
    Print_center(4,tempbuf);
    respWaitTime(2);
    while(checkTimerIsExpired() == TIMER_NOT_EXPIRED);
}

/*ISR for RTC_A*/
#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=RTC_VECTOR
__interrupt void RTC_ISR(void)
#elif defined(__GNUC__)
void __attribute__ ((interrupt(RTC_VECTOR))) RTC_ISR (void)
#else
#error Compiler not supported!
#endif
{
  switch(__even_in_range(RTCIV,16))
  {
    case 0:  break;                          // No interrupts
    case 2:  break;                          // RTCRDYIFG
    case 4:  break;                          // RTCEVIFG
    case 6: Alarm_flag = 1;
        _no_operation();
        __bic_SR_register_on_exit(LPM3_bits);
        _no_operation();
            break;                          // RTCAIFG
    case 8:  break;                          // RT0PSIFG
    case 10: break;                          // RT1PSIFG
    case 12: break;                          // Reserved
    case 14: break;                          // Reserved
    case 16: break;                          // Reserved
    default: break;
  }
}

