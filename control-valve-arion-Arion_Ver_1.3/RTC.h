/*
 * RTC.h
 *  Modified on : Feb 05,2020
 *  Created on  : Sep 25, 2020
 *      Author  : Arjun Shankar
 */

#ifndef RTC_H_
#define RTC_H_

#define DEFAULT_YEAR        2024
#define DEFAULT_MONTH       5
#define DEFAULT_DAY         28
#define DEFAULT_HOUR        23
#define DEFAULT_MIN         58
#define DEFAULT_SEC         00

void rtcinit(void);

void get_rtc(Calendar *Time);
int check_alarmwakeup(void);
void reset_alarmwakeup(void);

void change_RTCtime ( Calendar time2Save );
void set_next_alarm(void);

#endif /* RTC_H_ */
