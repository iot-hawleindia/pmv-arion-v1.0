/*
 * config.h
 *  Modified on : Feb 05,2020
 *  Created on  : Sep 25, 2020
 *      Author  : Arjun Shankar
 */

#include <display.h>
#include "driverlib.h"
#include <msp430.h>
#include "i2c.h"
#include "RTC.h"
#include "timer.h"
#include "wdt.h"
#include "gpio.h"
#include "Menu.h"
#include <stdio.h>
#include <stdint.h>

#ifndef CONFIG_H_
#define CONFIG_H_

//Version details
#define SW_VER                      "1.3"
#define HW_VER                      "1.0"

//Info memory addresses
#define ALARM_ADDR                 (0x1904)
#define INFO_ADDR                  (0x1900)
#define RTC_ADDR                   (0x1918)
#define CONFIG_VALUE               (0x11)

//Time delay
#define ONE_SEC                     1000000
#define DELAY_DEBOUNCE_INTERVAL     100000
#define FIFTY_MILLISECONDS          50000

//Menu positions
#define MENU_X                      16
#define PARAMETER_X                 80
#define CURSOR_X                    0

//Input states
enum INPUT
{
    ENTER = 1,
    UP = 2,
    DOWN = 3
};

//Relay states
enum STATE
{
    OFF = 1,
    ON = 2,
    DELAY = 3
};


enum BOOLEAN
{
    TRUE = 1,
    FALSE = 0
};


struct alarm
{
    uint8_t hours;
    uint8_t minutes;
    uint8_t relay;
    uint8_t Valvestate;
    uint8_t delay;
};

struct Infodata
{
    uint8_t start_address;
    uint8_t restart_act;
    uint8_t restart_flag;
    uint8_t current_alarm;
};

/* Display Home screen */
/*-----------------------------------------------------------------
NAME         : void homescreen(void);
PROCESS      : Print current time and date on display
-----------------------------------------------------------------*/
void homescreen(void);

/* Function to detect the input from User */
/*-----------------------------------------------------------------
NAME         : void readinput(void);
OUTPUTS      : sets flag to corresponding input from User
PROCESS      : Wait for User input and update the flag for type of input
-----------------------------------------------------------------*/
void readinput(void);

#endif /* CONFIG_H_ */
