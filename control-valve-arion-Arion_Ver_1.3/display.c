/*
 * display.c
 *  Modified on : Feb 05,2020
 *  Created on  : Sep 25, 2020
 *      Author  : Arjun Shankar
 */

#include "display.h"
#include <msp430.h>
#include <stdint.h>
#include <stdio.h>
#include "font_5x7.h"
#include "i2c.h"
#include "config.h"


const unsigned char HcenterUL[] = {                                           // Horizontally center number with separators on screen
                               0,                                       // 0 digits, not used but included to size array correctly
                               61,                                      // 1 digit
                               58,                                      // 2 digits
                               55,                                      // 3 digits
                               52,                                      // 4 digits and 1 separator
                               49,                                      // 5 digits and 1 separator
                               46,                                      // 6 digits and 1 separator
                               43,                                      // 7 digits and 2 separators
                               40,                                      // 8 digits and 2 separators
                               37,                                      // 9 digits and 2 separators
                               34                                       // 10 digits and 3 separators
};

void display_init(void)
{
    /*display init sequence*/

    display_command(display_DISPLAYOFF);
    display_command(display_SETDISPLAYCLOCKDIV);
    display_command(0x80);

    display_command(display_SETMULTIPLEX);
    display_command(display_LCDHEIGHT - 1);

    display_command(display_SETDISPLAYOFFSET);
    display_command(0x0);
    display_command(display_SETSTARTLINE | 0x0);
    display_command(display_CHARGEPUMP);
    display_command(0x14);
    display_command(display_MEMORYMODE);
    display_command(0x00);
    display_command(display_SEGREMAP | 0x1);
    display_command(display_COMSCANDEC);

    display_command(display_SETCOMPINS);
    display_command(0x12);
    display_command(display_SETCONTRAST);
    display_command(0xCF);

    display_command(display_SETPRECHARGE);
    display_command(0xF1);
    display_command(display_SETVCOMDETECT);
    display_command(0x40);
    display_command(display_DISPLAYALLON_RESUME);
    display_command(display_NORMALDISPLAY);

    display_command(display_DEACTIVATE_SCROLL);

    display_command(display_DISPLAYON);
    display_clearDisplay();
    //turn on oled panel
}

void display_command(unsigned char command)
{
    buffer[0] = 0x80;
    buffer[1] = command;

    i2c_write(buffer, 2);
}

void display_clearDisplay(void)
{

    display_setPosition(0, 0);
    uint8_t count;
    for (count = 64; count > 0; count--) {                                          // count down for loops
        uint8_t position;
        for(position = 16; position > 0; position--) {
            if (position == 1) {
                buffer[position-1] = 0x40;
            } else {
                buffer[position-1] = 0x0;
            }
        }

        i2c_write(buffer, 17);
    }
}

void display_setPosition(uint8_t column, uint8_t page) {
    if (column > 128) {
        column = 0;                                                     // constrain column to upper limit
    }

    if (page > 8) {
        page = 0;                                                       // constrain page to upper limit
    }

    display_command(display_COLUMNADDR);
    display_command(column);                                            // Column start address (0 = reset)
    display_command(display_LCDWIDTH-1);                                // Column end address (127 = reset)

    display_command(display_PAGEADDR);
    display_command(page);                                              // Page start address (0 = reset)
    display_command(7);                                                 // Page end address
}

void display_printText(uint8_t column_pos, uint8_t page_pos, char *ptString) {
    display_setPosition(column_pos, page_pos);

    while (*ptString != '\0') {
        buffer[0] = 0x40;

        if ((column_pos + 5) >= 127) {                                           // char will run off screen
            column_pos = 0;                                                      // set column to 0
            page_pos++;                                                        // jump to next page
            display_setPosition(column_pos, page_pos);                                  // send position change to oled
        }

        uint8_t counter;
        for(counter = 0; counter< 5; counter++) {
            buffer[counter+1] = font_5x7[*ptString - ' '][counter];
        }

        buffer[6] = 0x0;

        i2c_write(buffer, 7);
        ptString++;
        column_pos+=6;
    }
}

void Print_center(uint8_t row,char *ptString)
{
    unsigned int length = 0;
    char text[30]= {0};
    while(*ptString != '\0')
    {
        text[length] = *ptString;
        length++;
        ptString++;
    }
    text[length] = '\0';
    display_printText(HcenterUL[length],row,text);


}

void update_time(void)
{
    static Calendar displaytime={0};
    char time[20]={0},date[20]={0};

    /*get current time*/

    get_rtc(&displaytime);

    if((displaytime.Hours < 10) && (displaytime.Minutes < 10))
    {
        sprintf(time,"0%d:0%d",displaytime.Hours ,displaytime.Minutes );
    }
    else if(displaytime.Minutes < 10)
    {
        sprintf(time,"%d:0%d",displaytime.Hours ,displaytime.Minutes );
    }
    else if(displaytime.Hours<10)
    {
        sprintf(time,"0%d:%d",displaytime.Hours ,displaytime.Minutes );
    }
    else
    {
        sprintf(time,"%d:%d",displaytime.Hours ,displaytime.Minutes );
    }

    if((displaytime.DayOfMonth<10)&&(displaytime.Month<10))
    {
        sprintf(date,"0%d/0%d/%d",displaytime.DayOfMonth,displaytime.Month,displaytime.Year);
    }
    else if(displaytime.DayOfMonth<10)
    {
        sprintf(date,"0%d/%d/%d",displaytime.DayOfMonth,displaytime.Month,displaytime.Year);
    }
    else if(displaytime.Month<10 )
    {
        sprintf(date,"%d/0%d/%d",displaytime.DayOfMonth,displaytime.Month,displaytime.Year);
    }
    else
    {
        sprintf(date,"%d/%d/%d",displaytime.DayOfMonth,displaytime.Month,displaytime.Year);
    }

    /*display date*/
    Print_center(2,date);
    /*display time*/
    Print_center(4,time);
}
