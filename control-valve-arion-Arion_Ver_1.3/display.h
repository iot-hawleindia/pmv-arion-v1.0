/*
 * display.h
 *  Modified on : Feb 05,2020
 *  Created on  : Sep 25, 2020
 *      Author  : Arjun Shankar
 */

#ifndef DISPLAY_H_
#define DISPLAY_H_

#include <msp430.h>
#include <stdint.h>
#include <string.h>
#include "i2c.h"

unsigned char buffer[30]; // buffer for data transmission to screen

#define HCENTERUL_OFF   0
#define HCENTERUL_ON    1

#define display_I2C_ADDRESS             0x3C
#define display_LCDWIDTH                128
#define display_LCDHEIGHT               64
#define display_128_64
#define display_SETCONTRAST             0x81
#define display_DISPLAYALLON_RESUME     0xA4
#define display_DISPLAYALLON            0xA5
#define display_NORMALDISPLAY           0xA6
#define display_INVERTDISPLAY           0xA7
#define display_DISPLAYOFF              0xAE
#define display_DISPLAYON               0xAF

#define display_SETDISPLAYOFFSET        0xD3
#define display_SETCOMPINS              0xDA

#define display_SETVCOMDETECT           0xDB

#define display_SETDISPLAYCLOCKDIV      0xD5
#define display_SETPRECHARGE            0xD9

#define display_SETMULTIPLEX            0xA8

#define display_SETLOWCOLUMN            0x00
#define display_SETHIGHCOLUMN           0x10

#define display_SETSTARTLINE            0x40

#define display_MEMORYMODE              0x20
#define display_COLUMNADDR              0x21
#define display_PAGEADDR                0x22

#define display_COMSCANINC              0xC0
#define display_COMSCANDEC              0xC8

#define display_SEGREMAP                0xA0

#define display_CHARGEPUMP              0x8D

#define display_EXTERNALVCC             0x1
#define display_SWITCHCAPVCC            0x2

// currently no scroll functionality, left for possible future use
#define display_ACTIVATE_SCROLL                         0x2F
#define display_DEACTIVATE_SCROLL                       0x2E
#define display_SET_VERTICAL_SCROLL_AREA                0xA3
#define display_RIGHT_HORIZONTAL_SCROLL                 0x26
#define display_LEFT_HORIZONTAL_SCROLL                  0x27
#define display_VERTICAL_AND_RIGHT_HORIZONTAL_SCROLL    0x29
#define display_VERTICAL_AND_LEFT_HORIZONTAL_SCROLL     0x2A


/*-----------------------------------------------------------------
NAME        : void timerStop_delay(void);
PROCESS     : stop timer A0 module
-----------------------------------------------------------------*/
void display_init(void);
/*-----------------------------------------------------------------
NAME        : void timerStop_delay(void);
PROCESS     : stop timer A0 module
-----------------------------------------------------------------*/
void display_command(unsigned char);
/*-----------------------------------------------------------------
NAME        : void timerStop_delay(void);
PROCESS     : stop timer A0 module
-----------------------------------------------------------------*/
void display_clearDisplay(void);
/*-----------------------------------------------------------------
NAME        : void timerStop_delay(void);
PROCESS     : stop timer A0 module
-----------------------------------------------------------------*/
void display_setPosition(uint8_t, uint8_t);
/*-----------------------------------------------------------------
NAME        : void timerStop_delay(void);
PROCESS     : stop timer A0 module
-----------------------------------------------------------------*/
void display_printText(uint8_t, uint8_t, char *);
/*-----------------------------------------------------------------
NAME        : void timerStop_delay(void);
PROCESS     : stop timer A0 module
-----------------------------------------------------------------*/
void Print_center(uint8_t y,char *ptString);
/*-----------------------------------------------------------------
NAME        : void timerStop_delay(void);
PROCESS     : stop timer A0 module
-----------------------------------------------------------------*/
void update_time(void);


#endif /* DISPLAY_H_ */
