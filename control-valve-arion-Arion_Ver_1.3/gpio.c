/*
 * gpio.c
 *  Modified on : Feb 05,2020
 *  Created on  : Oct 12, 2020
 *      Author  : Arjun Shankar
 */
#include"gpio.h"

//To indicate if demand wake up is given
int demandwakeup_flag = 0;

//For differentiate the input button
extern int Input;

void gpio_init(void)
{
    /*set pin controlling MOSFET (to turn ON OFF OLED) display as output*/
    GPIO_setAsOutputPin(PORT_OLED,PIN_OLED);
    /*set low on display pin*/
    GPIO_setOutputLowOnPin(PORT_OLED,PIN_OLED);
    /*set pins to control relay as output*/
    GPIO_setAsOutputPin(PORT_RELAY,PIN_RELAY1+PIN_RELAY2+PIN_RELAY3+PIN_RELAY4);
    /*set low on relay pin*/
    GPIO_setOutputLowOnPin(PORT_RELAY,PIN_RELAY1+PIN_RELAY2+PIN_RELAY3+PIN_RELAY4);
    /*set alarm LED pins as output*/
    GPIO_setAsOutputPin(PORT_ALARM_LED,ALARM_LED1+ALARM_LED2+ALARM_LED3+ALARM_LED4);
    /*set low on Alarm LED pins*/
    GPIO_setOutputLowOnPin(PORT_ALARM_LED,ALARM_LED1+ALARM_LED2+ALARM_LED3+ALARM_LED4);
    /*set Debug LEDs as output pin*/
    GPIO_setAsOutputPin(DEBUG_LED,DEBUG_LED1+DEBUG_LED2);
    /*set low on Debug Leds*/
    GPIO_setOutputLowOnPin(DEBUG_LED,DEBUG_LED1+DEBUG_LED2);
    /*Set push button pins as input pin with pull up resistor*/
    GPIO_setAsInputPinWithPullUpResistor(PORT_INPUT,PIN_ENTER+PIN_UP+PIN_DOWN);
    /*set wakeup button pin as interrupt*/
    gpioIntrSet(PORT_INPUT,PIN_WAKEUP);
}

void gpioIntrSet(int Port, int Pin)
{
    /* Set the pin as Input  */
    GPIO_setAsInputPinWithPullUpResistor(Port, Pin);
    /* Set the pin to detect interrupt */
    GPIO_enableInterrupt(Port, Pin);
    /* Set the pin to detect falling edge*/
    GPIO_selectInterruptEdge(Port, Pin, GPIO_HIGH_TO_LOW_TRANSITION);
    /* Clear the interrupt */
    GPIO_clearInterrupt(Port, Pin);
}

void gpio_sleep(void)
{
    //unwanted pins are made as output low to conserve power during sleep
    GPIO_setAsOutputPin(GPIO_PORT_P1,GPIO_PIN_ALL8);
    //P2.1 is enabled as interrupt for wakeup button during sleep
    GPIO_setAsOutputPin(GPIO_PORT_P2,GPIO_PIN0+GPIO_PIN2+GPIO_PIN3+GPIO_PIN4+GPIO_PIN5+GPIO_PIN6+GPIO_PIN7);
    GPIO_setAsOutputPin(GPIO_PORT_P3,GPIO_PIN_ALL8);
    GPIO_setAsOutputPin(GPIO_PORT_P4,GPIO_PIN_ALL8);
    GPIO_setAsOutputPin(GPIO_PORT_P5,GPIO_PIN_ALL8);
    GPIO_setAsOutputPin(GPIO_PORT_P6,GPIO_PIN_ALL8);
    //P7.0,P7.1 are used for XT1 pins
    GPIO_setAsOutputPin(GPIO_PORT_P7,GPIO_PIN2+GPIO_PIN3+GPIO_PIN4+GPIO_PIN5+GPIO_PIN6+GPIO_PIN7);
    GPIO_setAsOutputPin(GPIO_PORT_P8,GPIO_PIN_ALL8);
    GPIO_setAsOutputPin(GPIO_PORT_P9,GPIO_PIN_ALL8);
    GPIO_setAsOutputPin(GPIO_PORT_P10,GPIO_PIN_ALL8);
    GPIO_setAsOutputPin(GPIO_PORT_P11,GPIO_PIN_ALL8);
    GPIO_setAsOutputPin(GPIO_PORT_PJ,GPIO_PIN_ALL8);

    GPIO_setOutputLowOnPin(GPIO_PORT_P1,GPIO_PIN_ALL8);
    //P2.1 is enabled as interrupt for wakeup button during sleep
    GPIO_setOutputLowOnPin(GPIO_PORT_P2,GPIO_PIN0+GPIO_PIN2+GPIO_PIN3+GPIO_PIN4+GPIO_PIN5+GPIO_PIN6+GPIO_PIN7);
    GPIO_setOutputLowOnPin(GPIO_PORT_P3,GPIO_PIN_ALL8);
    GPIO_setOutputLowOnPin(GPIO_PORT_P4,GPIO_PIN_ALL8);
    GPIO_setOutputLowOnPin(GPIO_PORT_P5,GPIO_PIN_ALL8);
    GPIO_setOutputLowOnPin(GPIO_PORT_P6,GPIO_PIN_ALL8);
    //P7.0,P7.1 are used for XT1 pins
    GPIO_setOutputLowOnPin(GPIO_PORT_P7,GPIO_PIN2+GPIO_PIN3+GPIO_PIN4+GPIO_PIN5+GPIO_PIN6+GPIO_PIN7);
    //P8.0,P8.1,P8.2,P8.3  are used for controlling relay and their state should not be changed automatically before sleep
    GPIO_setOutputLowOnPin(GPIO_PORT_P9,GPIO_PIN_ALL8);
    //GPIO_setOutputLowOnPin(GPIO_PORT_P8,GPIO_PIN4+GPIO_PIN5+GPIO_PIN6+GPIO_PIN7);
    GPIO_setOutputLowOnPin(GPIO_PORT_P9,GPIO_PIN_ALL8);
    GPIO_setOutputLowOnPin(GPIO_PORT_P10,GPIO_PIN_ALL8);
    GPIO_setOutputLowOnPin(GPIO_PORT_P11,GPIO_PIN_ALL8);
    GPIO_setOutputLowOnPin(GPIO_PORT_PJ,GPIO_PIN_ALL8);
}

int check_demandwakeup(void)
{
    return demandwakeup_flag;
}

void reset_demandwakeupflag(void)
{
    demandwakeup_flag = 0;
}

//GPIO - ISR
#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=PORT2_VECTOR
__interrupt void Port_2(void)
#else
#error Compiler not supported!
#endif
{
    switch(__even_in_range(P2IV,16))
    {
    /* No interrupt*/
    case  0: break;
    /* P2IV P2IFG.0 */
    case  2: break;
    /* P2IV P2IFG.1 */
    case  4:
        //set flag to indicate demandwakeup button is pressed
        demandwakeup_flag = 1;
        __no_operation();
        //exit LPM3
        __bic_SR_register_on_exit(LPM3_bits);
        _no_operation();

            break;
    /* P2IV P2IFG.2 */
    case  6: break;
    /* P2IV P2IFG.3 */
    case  8: break;
    /* P2IV P2IFG.4 */
    case 10: break;
    /* P2IV P2IFG.5 */
    case 12: break;
    /* P2IV P2IFG.6 */
    case 14: break;
    /* P2IV P2IFG.7 */
    case 16: break;

    default: break;
    }

}

