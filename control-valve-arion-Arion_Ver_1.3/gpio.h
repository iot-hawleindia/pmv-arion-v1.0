/*
 * gpio.h
 *  Modified on : Feb 05,2020
 *  Created on  : Oct 12, 2020
 *      Author  : Arjun Shankar
 */

#ifndef GPIO_H_
#define GPIO_H_

#include "Config.h"

#define PORT_XT1        GPIO_PORT_P7
#define PIN_XT1_IN      GPIO_PIN0
#define PIN_XT1_OUT     GPIO_PIN1

#define PORT_INPUT      GPIO_PORT_P2
#define PIN_WAKEUP      GPIO_PIN1
#define PIN_ENTER       GPIO_PIN2
#define PIN_UP          GPIO_PIN4
#define PIN_DOWN        GPIO_PIN5

#define PORT_OLED       GPIO_PORT_P8
#define PIN_OLED        GPIO_PIN7

#define PORT_RELAY      GPIO_PORT_P8
#define PIN_RELAY1      GPIO_PIN3
#define PIN_RELAY2      GPIO_PIN2
#define PIN_RELAY3      GPIO_PIN1
#define PIN_RELAY4      GPIO_PIN0

#define PORT_ALARM_LED  GPIO_PORT_P6
#define ALARM_LED1      GPIO_PIN3
#define ALARM_LED2      GPIO_PIN2
#define ALARM_LED3      GPIO_PIN1
#define ALARM_LED4      GPIO_PIN0

#define DEBUG_LED       GPIO_PORT_P1
#define DEBUG_LED1      GPIO_PIN2
#define DEBUG_LED2      GPIO_PIN3




/*-----------------------------------------------------------------
NAME        : void gpio_init(void);
PROCESS     : GPIO pins are configured for the application
-----------------------------------------------------------------*/
void gpio_init(void);
/*-----------------------------------------------------------------
NAME        : void gpio_sleep(void);
PROCESS     : Configure GPIO pins before sleep to conserve power
-----------------------------------------------------------------*/
void gpio_sleep(void);
/*-----------------------------------------------------------------
NAME        : int check_demandwakeup(void);
OUTPUT      : Value of Demand wakeup flag
PROCESS     : check the value of demandwakeup flag
-----------------------------------------------------------------*/
int check_demandwakeup(void);
/*-----------------------------------------------------------------
NAME        : void reset_demandwakeupflag(void);
PROCESS     : Reset the demandwakeup flag
-----------------------------------------------------------------*/
void reset_demandwakeupflag(void);
/*-----------------------------------------------------------------
NAME        : void gpioIntrSet(int Port, int Pin);
PROCESS     : Configure the GPIO pin as Interrupt pin
-----------------------------------------------------------------*/
void gpioIntrSet(int Port, int Pin);


#endif /* GPIO_H_ */
