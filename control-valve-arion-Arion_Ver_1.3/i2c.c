/*
 * i2c.c
 *  Modified on : Feb 05,2020
 *  Created on  : Sep 25, 2020
 *      Author  : Arjun Shankar
 */
#include "i2c.h"

//slave address:78(bit shifted)
#define SLAVE_ADDR 0x3C

unsigned char *PTxData;                     // Pointer to TX data
unsigned char TXByteCtr;


void i2c_init(void)
{
    //Enable power to OLED display
    GPIO_setOutputHighOnPin(PORT_OLED,PIN_OLED);

    //small wait
    __delay_cycles(1000000);

    /*select I2C pins as special pins*/
    GPIO_setAsPeripheralModuleFunctionInputPin(GPIO_PORT_P9, GPIO_PIN1 + GPIO_PIN2);

    /*struct for initializing I2C*/
    USCI_B_I2C_initMasterParam param0 = {0};

    /*Configure SMCLK as source for I2C operation*/
    param0.selectClockSource = USCI_B_I2C_CLOCKSOURCE_SMCLK;
    /*calculated value to attain near 100kHz frequency*/
    param0.i2cClk = 1200000;
    /*set 100KBPS as data rate*/
    param0.dataRate = USCI_B_I2C_SET_DATA_RATE_100KBPS;

    /*initialize I2C module*/
    USCI_B_I2C_initMaster( USCI_B2_BASE, &param0);
    /*set slave address*/
    USCI_B_I2C_setSlaveAddress(USCI_B2_BASE,SLAVE_ADDR);
    /*enable I2C operation*/
    USCI_B_I2C_enable(USCI_B2_BASE);
    /*enable I2C transmit interrupt*/
    USCI_B_I2C_enableInterrupt (USCI_B2_BASE,USCI_B_I2C_TRANSMIT_INTERRUPT);

}


void i2c_write(unsigned char *DataBuffer, unsigned char ByteCtr)
{
    __delay_cycles(10);                         // small wait

    PTxData = DataBuffer;                       // TX array start address
    TXByteCtr = ByteCtr;                        // TX byte counter

    while (UCB2CTL1 & UCTXSTP);                 // Check Stop condition got sent
    UCB2CTL1 |= UCTR + UCTXSTT;                 // I2C TX, start condition

    __bis_SR_register(LPM0_bits + GIE);         // Enter LPM0, enable interrupts
    __no_operation();                           // Remain in LPM0 until all data is TX'd
}

void i2c_disable()
{
    USCI_B_I2C_disable(USCI_B2_BASE);
}

//I2C ISR
#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector = USCI_B2_VECTOR
__interrupt void USCI_B2_ISR(void)
#elif defined(__GNUC__)
void __attribute__ ((interrupt(USCI_B2_VECTOR))) USCI_B2_ISR (void)
#else
#error Compiler not supported!
#endif
{
  switch(__even_in_range(UCB2IV,12))
  {
  case  0: break;                           // Vector  0: No interrupts
  case  2: break;                           // Vector  2: ALIFG
  case  4: break;                           // Vector  4: NACKIFG
  case  6: break;                           // Vector  6: STTIFG
  case  8: break;                           // Vector  8: STPIFG
  case 10: break;                           // Vector 10: RXIFG
  case 12:                                  // Vector 12: TXIFG
    if (TXByteCtr)                          // Check TX byte counter
    {
      UCB2TXBUF = *PTxData++;               // Load next TX address
      TXByteCtr--;                          // Decrement TX byte counter
    }
    else
    {
      UCB2CTL1 |= UCTXSTP;                  // I2C stop condition
      UCB2IFG &= ~UCTXIFG;                  // Clear USCI_B1 TX int flag
      __bic_SR_register_on_exit(LPM0_bits); // Exit LPM0
      __no_operation();
    }
  default: break;
  }
}
