/*
 * i2c.h
 *  Modified on : Feb 05,2020
 *  Created on  : Sep 25, 2020
 *      Author  : Arjun Shankar
 */
#ifndef I2C_H_
#define I2C_H_

#include "Config.h"

/*-----------------------------------------------------------------
NAME        : void i2c_init(void);
PROCESS     : Initialize I2C module for communication with OLED slave
-----------------------------------------------------------------*/
void i2c_init(void);
/*-----------------------------------------------------------------
NAME        : void i2c_write(unsigned char *, unsigned char);
PROCESS     : write data to I2C bus
-----------------------------------------------------------------*/
void i2c_write(unsigned char *, unsigned char);
/*-----------------------------------------------------------------
NAME        : void i2c_disable();
PROCESS     : Disable I2C module
-----------------------------------------------------------------*/
void i2c_disable();

#endif /* I2C_H_ */
