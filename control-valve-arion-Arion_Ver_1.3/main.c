/*
 * main.c
 *  Modified on : Feb 05,2020
 *  Created on  : Sep 25, 2020
 *      Author  : Arjun Shankar
 */

#include "config.h"

/*global variables*/

/*Flag to identify the type of input from User*/
int Input = 0;
/*Flag to identify the device startup*/
int startup = 0;
/*identifier for SYSRSTIV*/
int reset_source = 0;
/*flag to indicate if the exit option is selected*/
int exit_flag = 0;
/*struct to maintain four alarm parameters*/
struct alarm alarm[4];
/*struct to maintain parameters need after wake up*/
struct Infodata Info;
/*struct to store backup time*/
Calendar backup;
/*temp buffer*/
char reset[30]={0};
/*flag to identify if the input is given while on homescreen*/
bool HomeScreen = 0;

//Application Functions

/* CV application function */
/*-----------------------------------------------------------------
NAME         : void CV_task(void);
PROCESS      : Main Loop that perform the device functions
-----------------------------------------------------------------*/
void CV_task(void);
/* Sleep settings configuration and Entry into LPM3 to conserve power   */
/*-----------------------------------------------------------------
NAME         : void goto_sleep(void);
PROCESS      : Prepares Device for entering Low power mode
-----------------------------------------------------------------*/
void goto_sleep(void);
/* functions to perform after demand/alarm wake up from LPM */
/*-----------------------------------------------------------------
NAME         : void wakeup(void);
PROCESS      : Initializes peripherals after wake up from LPM
-----------------------------------------------------------------*/
void wakeup(void);
/* Print the next calculated alarm time on display before entering LPM */
/*-----------------------------------------------------------------
NAME         : void print_alarm(void);
PROCESS      : Calculates the next alarm time from four different alarms and prints on the display
-----------------------------------------------------------------*/
void print_alarm(void);
/* Relay operation performed during alarm wake up */
/*-----------------------------------------------------------------
NAME         : void alarm_task();
PROCESS      : Identify and perform the relay operation for the alarm wake up based on the user configuration
-----------------------------------------------------------------*/
void alarm_task(void);
/* Checks if the Info memory is configured already */
/*-----------------------------------------------------------------
NAME         : static int is_Device_Configured(void);
OUTPUTS      : True if the info memory is configured or else false
PROCESS      : checks the value at the start address and returns the status
-----------------------------------------------------------------*/
static int is_Device_Configured(void);
/* write the values passed in Info memory  */
/*-----------------------------------------------------------------
NAME         : void write_info(uint8_t *data_ptr, uint8_t *flash_ptr, uint16_t count);
INPUTS       : data to be written, info memory start address, total size of the data
PROCESS      : Write Function for flash
-----------------------------------------------------------------*/
void write_info(uint8_t *data_ptr, uint8_t *flash_ptr, uint16_t count);
/* Copy values from local variables and write them in Info memory */
/*-----------------------------------------------------------------
NAME         : void local_to_info(void);
PROCESS      : writes the alarm parameters, backup time, Info parameters in Info memory copying from Local variables
-----------------------------------------------------------------*/
void local_to_info(void);
/* write alarm parameters into Information memory  */
/*-----------------------------------------------------------------
NAME         : static void  FlashWrite_alarm(struct alarm *pConfig,int i, int Totalbytes);
INPUTS       : Buffer having the values to be written in Info memory and size of the buffer
PROCESS      : write alarm parameters time in Info memory
-----------------------------------------------------------------*/
static void  FlashWrite_alarm(struct alarm *pConfig,int i, int Totalbytes);
/* write Info parameters into Information memory  */
/*-----------------------------------------------------------------
NAME         : static void  FlashWrite_info(struct Infodata *pConfig, int Totalbytes);
INPUTS       : Buffer having the values to be written in Info memory and size of the buffer
PROCESS      : write Info parameters time in Info memory
-----------------------------------------------------------------*/
static void  FlashWrite_info(struct Infodata *pConfig, int Totalbytes);
/* write RTC backup time into Information memory  */
/*-----------------------------------------------------------------
NAME         : static void  FlashWrite_RTC(struct Calendar  *pConfig, int Totalbytes);
INPUTS       : Buffer having the values to be written in Info memory and size of the buffer
PROCESS      : write backup time in Info memory
-----------------------------------------------------------------*/
static void  FlashWrite_RTC(struct Calendar  *pConfig, int Totalbytes);
/* Copy alarm parameters from Information memory  */
/*-----------------------------------------------------------------
NAME         : static void copyflash_alarm(struct alarm *pConfig,int i, int Totalbytes);
INPUTS       : Temp buffer to store the values from Info memory and size of the buffer
OUTPUTS      : Alarm parameters stored in temp buffer passed
PROCESS      : copy the alarm parameters stored in info memory to temp buffer
-----------------------------------------------------------------*/
static void copyflash_alarm(struct alarm *pConfig,int i, int Totalbytes);
/* Copy Info parameters from Information memory  */
/*-----------------------------------------------------------------
NAME         : static void copyflash_Info(struct Infodata *pConfig, int Totalbytes);
INPUTS       : Temp buffer to store the values from Info memory and size of the buffer
OUTPUTS      : Info values stored in temp buffer passed
PROCESS      : copy the Info parameters stored in info memory to temp buffer
-----------------------------------------------------------------*/
static void copyflash_Info(struct Infodata *pConfig, int Totalbytes);
/* Read and copy Backup time  from Info memory  */
/*-----------------------------------------------------------------
NAME         : static void copyflash_RTC(struct Calendar  *pConfig, int Totalbytes);
INPUTS       : Temp buffer to store the values from Info memory and size of the buffer
OUTPUTS      : Backup time stored in the temp buffer passed
PROCESS      : copy the RTC backup time stored in info memory to temp buffer
-----------------------------------------------------------------*/
static void copyflash_RTC(struct Calendar  *pConfig, int Totalbytes);
/* Copy values from Info memory to Local variables */
/*-----------------------------------------------------------------
NAME         : void info_to_local(void);
PROCESS      : Reads and copies values stored in info memory to corresponding local variables
-----------------------------------------------------------------*/
void info_to_local(void);

/* assign default values to variables at start up */
/*-----------------------------------------------------------------
NAME         : void Default_values(void);
PROCESS      : assign values to local variables at the time of startup to avoid garbage values
-----------------------------------------------------------------*/
void Default_values(void);

bool checkXT1fault(void);


/*Main Function*/

int main(void)
{
    /*configure watchdog timer within 32ms to avoid reset*/
    wdt_init();

    /*Turn XT1 ON - 32kHz clock for sourcing RTC module*/
    turnOnXt1Clock();

    /*Enable General interrupts*/
    __bis_SR_register(GIE);

    /*Initialize functions*/
    gpio_init();            //Gpio Initialization
    rtcinit();              //RTC module to maintain time
    i2c_init();             //I2C module to communicate with display
    timerInit();            //Timer initialization
    display_init();         //initialize display for displaying content

    /*Flag to indicate Device startup*/
    startup = 1;

    /*get the reset id*/
    display_clearDisplay();
    //copy the reset source number
    reset_source = SYSRSTIV;
    //set default values to global variables
    Default_values();

    //check if the info memory is written
    if (is_Device_Configured() == FALSE)
    {
        /*if not written already , write to info memory
        copy values from local variables to info memory*/
        local_to_info();
    }
    else
    {
        /*if written already , write to local variables
        copy values to variables from Info memory*/
        info_to_local();
    }

    /*if the restart act is not configured then the RTC is loaded with Backup time after restart*/
    if((reset_source != 0))
    {
        if(Info.restart_act == 0)
        {
            change_RTCtime(backup);
        }
        else
        {
            Info.restart_flag = 1;

        }
    }
    set_next_alarm();

    notifyWDT();
    /*Initial one time startup message*/
    startup_message();

    notifyWDT();
    //print the reset source
    sprintf(reset,"reset: 0x%x",reset_source);
    Print_center(2,reset);
    respWaitTime(2);
    while(checkTimerIsExpired() == TIMER_NOT_EXPIRED);

    //Start Application Task
    CV_task();

}
void Default_values(void)
{
    unsigned int i=0;
    Info.start_address = CONFIG_VALUE;
    Info.current_alarm = 0;
    Info.restart_act = 0;
    Info.restart_flag = 0;

    for (i=0;i<4;i++)
    {
        alarm[i].hours = 0;
        alarm[i].minutes = 0;
        alarm[i].relay = 0;
        alarm[i].Valvestate = 3;
        alarm[i].delay = 250;
    }
}
void info_to_local(void)
{
    copyflash_Info(& Info,sizeof(Info));
        unsigned int i=0;
        for (i=0;i<4;i++)
        {
        copyflash_alarm(& alarm[i],i,sizeof(alarm[i]));
        }
    copyflash_RTC(& backup ,sizeof(backup));

}

static void copyflash_Info(struct Infodata *pConfig, int Totalbytes)
{
    memcpy((void *)pConfig,(void *) INFO_ADDR,Totalbytes);
}

static void copyflash_alarm(struct alarm *pConfig,int i, int Totalbytes)
{
    memcpy((void *)pConfig,(void *) (ALARM_ADDR+(i*5)),Totalbytes);
}

static void copyflash_RTC(struct Calendar  *pConfig, int Totalbytes)
{
    memcpy((void *)pConfig,(void *) RTC_ADDR,Totalbytes);
}

void local_to_info(void)
{

    uint16_t status;

    do
    {
        //Erase Info Memory [128 Bytes] INFOB
        FlashCtl_eraseSegment((uint8_t *)INFO_ADDR);
        status = FlashCtl_performEraseCheck((uint8_t *)INFO_ADDR,128);
    }
    while(status == STATUS_FAIL);
    FlashWrite_info(& Info,sizeof(Info));

    unsigned int i=0;
    for (i=0;i<4;i++)
    {
        FlashWrite_alarm(& alarm[i],i,sizeof(alarm[i]));
    }

    get_rtc(&backup);
    FlashWrite_RTC(& backup,sizeof(backup));

}

static void  FlashWrite_RTC(struct Calendar  *pConfig, int Totalbytes)
{
    write_info( (uint8_t *)pConfig,(uint8_t *)(RTC_ADDR),Totalbytes/sizeof(uint8_t));
}

static void  FlashWrite_info(struct Infodata *pConfig, int Totalbytes)
{
    write_info( (uint8_t *)pConfig,(uint8_t *)(INFO_ADDR),Totalbytes/sizeof(uint8_t));
}


static void  FlashWrite_alarm(struct alarm *pConfig,int i, int Totalbytes)
{
    write_info( (uint8_t *)pConfig,(uint8_t *)(ALARM_ADDR+(i*5)),Totalbytes/sizeof(uint8_t));
}

void write_info(uint8_t *data_ptr, uint8_t *flash_ptr, uint16_t count)
{
    while (count != 0 )
    {
        /* Write data into the flash memory in 32-bit word format */
        FlashCtl_write8(data_ptr++,flash_ptr++,1);
        count--;
    }
}

static int is_Device_Configured(void)
{
    uint8_t *Flash_ptr;

    /* Flash configuration address */
    Flash_ptr = (uint8_t*)(INFO_ADDR);
    /* Check Already configuration present */
    if(*Flash_ptr == CONFIG_VALUE)
    {
        /* Indicate configuration data is already present */
        return TRUE;
    }
    /* Indicate configuration data is not present */
    return FALSE;
}

int check_startup()
{
    return startup;
}

void CV_task(void)
{
   /*continuous Application task*/
   while(1)
   {

       /*initial Home screen with current time and date*/
       homescreen();
       //set next alarm
       if(check_alarmwakeup())
       {
           display_clearDisplay();
           reset_alarmwakeup();
           //message to indicate the alarm wakeup
           Print_center(6, "ALARM WAKEUP");
           respWaitTime(2);
           while(checkTimerIsExpired() == TIMER_NOT_EXPIRED);
           alarm_task();
       }
       set_next_alarm();
       print_alarm();
       //go to sleep
       goto_sleep();
       wakeup();

   }
}
void homescreen(void)
{
    /*clear display before showing new content*/
    display_clearDisplay();

    /*accessing Menu will available during startup and demand wakeup*/
    if(check_demandwakeup()||check_startup()||(exit_flag == 1))
    {
        HomeScreen = 1;

        if(checkXT1fault()==TRUE)
        {
            notifyWDT();
            Print_center(6, "XT1 Fault Occured");
            respWaitTime(2);
            while(checkTimerIsExpired() == TIMER_NOT_EXPIRED);
            display_clearDisplay();
        }
        update_time();

        Print_center(6, "Press Enter For MENU");

        /*reset startup flag*/
        startup = 0;
        /*reset demand wakeup flag*/
        reset_demandwakeupflag();

        exit_flag = 0;
        /*Set timer to 30 seconds*/
        respWaitTime(30);
        /*wait for user input*/
        readinput();

        HomeScreen = 0;
        /*enter MENU only if enter is pressed*/
        if(Input == ENTER )
        {
            /*clear display before showing new content*/
            display_clearDisplay();
            /*enter Menu*/
            menu(1);
        }

    }
}

void alarm_task()
{
    display_clearDisplay();
    switch(Info.current_alarm + 1)
    {
    case 1:GPIO_setOutputHighOnPin(PORT_ALARM_LED,ALARM_LED1);
            break;
    case 2:GPIO_setOutputHighOnPin(PORT_ALARM_LED,ALARM_LED2);
            break;
    case 3:GPIO_setOutputHighOnPin(PORT_ALARM_LED,ALARM_LED3);
            break;
    case 4:GPIO_setOutputHighOnPin(PORT_ALARM_LED,ALARM_LED4);
            break;
    default:
            break;
    }
    notifyWDT();
    relay_operation(alarm[Info.current_alarm]);

}

void goto_sleep(void)
{

    //copy variables from local to flash
    local_to_info();

    stopWdt();
    display_clearDisplay();
    Print_center(6, "Entering Sleep");
    respWaitTime(2);
    while(checkTimerIsExpired() == TIMER_NOT_EXPIRED);
    display_clearDisplay();
    i2c_disable();
    GPIO_setOutputLowOnPin(PORT_OLED,PIN_OLED);
    timerShutDown();
    timerShutDown_delay();
    gpio_sleep();

    __bis_SR_register(LPM3_bits + GIE);
    _no_operation();

}

void wakeup(void)
{
    wdt_init();
    gpio_init();
    i2c_init();
    timerInit();
    display_init();
    /*copy values to variables from Info memory*/
    info_to_local();
}


void readinput(void)
{
    Calendar time_update;
    static uint8_t prev_time = 0;
    Input = 0;
    /*check inputs untill timer expires or input change detected*/
    while((checkTimerIsExpired() == TIMER_NOT_EXPIRED) && (Input == 0))
    {
        notifyWDT();
        if(GPIO_getInputPinValue(PORT_INPUT,PIN_ENTER) == 0)
        {
            __delay_cycles(DELAY_DEBOUNCE_INTERVAL);
                if(GPIO_getInputPinValue(PORT_INPUT,PIN_ENTER) == 0)
                {
                    Input = ENTER;
                }
        }
        if(GPIO_getInputPinValue(PORT_INPUT,PIN_UP) == 0)
        {
            __delay_cycles(DELAY_DEBOUNCE_INTERVAL);
                if(GPIO_getInputPinValue(PORT_INPUT,PIN_UP) == 0)
                {
                    Input = UP;
                }
        }
        if(GPIO_getInputPinValue(PORT_INPUT,PIN_DOWN) == 0)
        {
            __delay_cycles(DELAY_DEBOUNCE_INTERVAL);
                if(GPIO_getInputPinValue(PORT_INPUT,PIN_DOWN) == 0)
                {
                    Input = DOWN;
                }
        }
        if(HomeScreen == 1)
        {
            get_rtc(&time_update);
            if(time_update.Minutes != prev_time)
            {
              update_time();
            }
            prev_time  = time_update.Minutes;

            if(Input == DOWN || Input == UP)
            {
                Input = 0;
            }
        }

 /************it has implemented for if user working in UI if alarm wake up will occur solenoid should actuate 22/3/2021 sridhar***/

        if(check_alarmwakeup())
        {
            reset_alarmwakeup();
            alarm_task();
            set_next_alarm();
            Input = UP;      // this is for after wake up message it should refresh the screen
            if(HomeScreen == 1)
            {
                Input = ENTER;
            }
        }

    }

}


bool checkXT1fault(void)
{
    int faultStatus = 0;
    UCS_clearFaultFlag(UCS_XT1LFOFFG);
    faultStatus = UCS_getFaultFlagStatus(UCS_XT1LFOFFG);
    if(faultStatus)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

