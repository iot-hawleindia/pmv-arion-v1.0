/*
 * timer.c
 *  Modified on : Feb 05,2020
 *  Created on  : Sep 26, 2020
 *      Author  : Arjun Shankar
 */
#include "config.h"
#include "timer.h"

/*Global variables*/
/* Initial status of timer */
volatile uint8_t timerStatus = TIMER_NOT_EXPIRED;
volatile uint8_t delay_timerStatus = TIMER_NOT_EXPIRED;
/* Initial status of Expire time */
volatile uint16_t expireTime = 0;
volatile uint16_t delay_expireTime = 0;
/* Initial status of start timer */
volatile uint16_t startTime = 0;
volatile uint16_t delay_startTime = 0;

void timerInit(void)
{
    /*clear interrupt before configuring timer module*/
    Timer_A_clearTimerInterrupt(TIMER_A1_BASE);

    /*struct for configuring timer A1*/
    Timer_A_initContinuousModeParam param = {0};
    /*select Aclk as Clock source*/
    param.clockSource = TIMER_A_CLOCKSOURCE_ACLK;
    /*calculated prescalar value for generating interrupt every 2 secs*/
    param.clockSourceDivider = TIMER_A_CLOCKSOURCE_DIVIDER_1;
    /*enable overflow interrupt */
    param.timerInterruptEnable_TAIE = TIMER_A_TAIE_INTERRUPT_ENABLE;
    /*clear timer count at start*/
    param.timerClear = TIMER_A_DO_CLEAR;
    /*do not start timer immediately*/
    param.startTimer = false;

    /* Set as continuous mode, to trigger the interrupt
     * every 2sec.
     */
    Timer_A_initContinuousMode(TIMER_A1_BASE, &param);
}

void timerInit_delay(void)
{
    /*clear interrupt before configuring timer module*/
    Timer_A_clearTimerInterrupt(TIMER_A0_BASE);
    /*struct for configuring timer A0*/
    Timer_A_initUpModeParam param1 = {0};
    /*select Aclk as Clock source*/
    param1.clockSource = TIMER_A_CLOCKSOURCE_ACLK;
    /*calculated prescalar value for generating interrupt every 5m secs*/
    param1.clockSourceDivider = TIMER_A_CLOCKSOURCE_DIVIDER_1;
    /*calculated timer value for generating interrupt every 5m secs*/
    param1.timerPeriod = 164;
    /*enable CCR0 interrupt */
    param1.timerInterruptEnable_TAIE = TIMER_A_TAIE_INTERRUPT_ENABLE;
    /*clear timer count at start*/
    param1.timerClear = TIMER_A_DO_CLEAR;
    /*do not start timer immediately*/
    param1.startTimer = false;

    /* Set as up mode, to trigger the interrupt
     * every 5ms.
     */
    Timer_A_initUpMode(TIMER_A0_BASE, &param1);
}


void timerStart ( uint16_t respTimeToExp )
{
    /*stop the timer before next iteration*/
    Timer_A_stop(TIMER_A1_BASE);
    /*set timer status flag*/
    timerStatus = TIMER_NOT_EXPIRED;
    /*set expire time*/
    expireTime = respTimeToExp;
    /*set timer value to 0*/
    startTime = 0;
    /*start timer A in continous mode*/
    Timer_A_startCounter(TIMER_A1_BASE,TIMER_A_CONTINUOUS_MODE);
}


void timerStart_delay ( uint16_t respTimeToExp )
{
    /*stop the timer before next iteration*/
    Timer_A_stop(TIMER_A0_BASE);
    /*set timer status flag*/
    delay_timerStatus = TIMER_NOT_EXPIRED;
    /*set expire time*/
    delay_expireTime = respTimeToExp;
    /*set timer value to 0*/
    delay_startTime = 0;
    /*start timer A in continous mode*/
    Timer_A_startCounter(TIMER_A0_BASE,TIMER_A_UP_MODE);
}

void timerStop(void)
{
    Timer_A_stop(TIMER_A1_BASE);
}

void timerStop_delay(void)
{
    Timer_A_stop(TIMER_A0_BASE);
}

/*set timer value*/
void respWaitTime ( uint16_t respTime )
{
    timerStart ( respTime );
}

void respWaitTime_delay ( uint16_t respTime )
{
    timerStart_delay ( respTime );
}

uint16_t checkTimerIsExpired ( void )
{
    return timerStatus;
}

uint16_t checkTimerIsExpired_delay ( void )
{
    return delay_timerStatus;
}

void timerShutDown(void)
{
    Timer_A_stop(TIMER_A1_BASE);
    Timer_A_clear(TIMER_A1_BASE);
    TA1CTL = 0x0000;
}

void timerShutDown_delay(void)
{
    Timer_A_stop(TIMER_A0_BASE);
    Timer_A_clear(TIMER_A0_BASE);
    TA0CTL = 0x0000;
}

/******************************************************************************
//
//This is the TIMER1_A1 interrupt vector service routine.
//
******************************************************************************/
#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=TIMER1_A1_VECTOR
__interrupt
#elif defined(__GNUC__)
__attribute__((interrupt(TIMER1_A1_VECTOR)))
#endif
void TIMER1_A1_ISR(void)
{
    /* Any access, read or write, of the TAIV register automatically resets the highest "pending" interrupt flag */
    switch(__even_in_range(TA1IV,14))
    {
    /* No interrupt*/
    case  0: break;
    /* CCR1 not used */
    case  2: break;
    /* CCR2 not used */
    case  4: break;
    /* CCR3 not used */
    case  6: break;
    /* CCR4 not used */
    case  8: break;
    /* CCR5 not used */
    case 10: break;
    /* CCR6 not used */
    case 12: break;
    case 14:
        /*each interrupt indicates 2s time expire*/
        startTime +=2;
        /*check if the time has lapsed*/
        if(startTime >= expireTime)
        {
            /*stop timer*/
            timerStop();
            /*set timer status flag*/
            timerStatus= TIMER_EXPIRED;
        }
        break;
    default: break;
    }
}

#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=TIMER0_A1_VECTOR
__interrupt
#elif defined(__GNUC__)
__attribute__((interrupt(TIMER1_A1_VECTOR)))
#endif
void TIMER0_A1_ISR(void)
{
    /* Any access, read or write, of the TAIV register automatically resets the highest "pending" interrupt flag */
    switch(__even_in_range(TA0IV,14))
    {
    /* No interrupt*/
    case  0: break;
    /* CCR1 not used */
    case  2: break;
    /* CCR2 not used */
    case  4: break;
    /* CCR3 not used */
    case  6: break;
    /* CCR4 not used */
    case  8: break;
    /* CCR5 not used */
    case 10: break;
    /* CCR6 not used */
    case 12: break;
    case 14:
        /*each interrupt indicates 5ms time expire*/
        delay_startTime +=5;
        /*check if the time has lapsed*/
        if(delay_startTime >= delay_expireTime)
        {
            /*stop timer*/
            timerStop_delay();
            /*set timer status flag*/
            delay_timerStatus= TIMER_EXPIRED;
        }
        break;
    default: break;
    }
}




