/*
 * timer.h
 *  Modified on : Feb 05,2020
 *  Created on  : Sep 26, 2020
 *      Author  : Arjun Shankar
 */

#ifndef TIMER_H_
#define TIMER_H_


#include <stdint.h>

enum TIMERSTATUS
{
    TIMER_NOT_EXPIRED = 0,
    TIMER_EXPIRED = 1
};

/*-----------------------------------------------------------------
NAME        : void timerInit(void);
PROCESS     : initialize Timer A1 module configured for seconds
-----------------------------------------------------------------*/
void timerInit(void);
/*-----------------------------------------------------------------
NAME        : void respWaitTime ( uint16_t respTime );
PROCESS     : set the timer A1 value to generate interrupt
-----------------------------------------------------------------*/
void respWaitTime ( uint16_t respTime );
/*-----------------------------------------------------------------
NAME        : uint16_t checkTimerIsExpired ( void );
PROCESS     : Check if the timer A1 has lapsed
-----------------------------------------------------------------*/
uint16_t checkTimerIsExpired ( void );
/*-----------------------------------------------------------------
NAME        : void timerShutDown(void);
PROCESS     : Disable Timer A1 module
-----------------------------------------------------------------*/
void timerShutDown(void);
/*-----------------------------------------------------------------
NAME        : void timerInit_delay(void);
PROCESS     : Initialize Timer A0 for millisecond operations
-----------------------------------------------------------------*/
void timerInit_delay(void);
/*-----------------------------------------------------------------
NAME        : void respWaitTime_delay ( uint16_t respTime );
PROCESS     : set timer A0 to count for the given time in milliseconds
-----------------------------------------------------------------*/
void respWaitTime_delay ( uint16_t respTime );
/*-----------------------------------------------------------------
NAME        : uint16_t checkTimerIsExpired_delay ( void );
PROCESS     : Check if the timer A0 has lapsed
-----------------------------------------------------------------*/
uint16_t checkTimerIsExpired_delay ( void );
/*-----------------------------------------------------------------
NAME        : void timerShutDown_delay(void);
PROCESS     : Disable timer A0 module
-----------------------------------------------------------------*/
void timerShutDown_delay(void);
/*-----------------------------------------------------------------
NAME        : void timerStart ( uint16_t respTimeToExp );
PROCESS     : start timer A1 module
-----------------------------------------------------------------*/
void timerStart ( uint16_t respTimeToExp );
/*-----------------------------------------------------------------
NAME        : void timerStart_delay ( uint16_t respTimeToExp );
PROCESS     : start timer A0 module
-----------------------------------------------------------------*/
void timerStart_delay ( uint16_t respTimeToExp );
/*-----------------------------------------------------------------
NAME        : void timerStop(void);
PROCESS     : stop Timer A1 module
-----------------------------------------------------------------*/
void timerStop(void);
/*-----------------------------------------------------------------
NAME        : void timerStop_delay(void);
PROCESS     : stop timer A0 module
-----------------------------------------------------------------*/
void timerStop_delay(void);


#endif /* TIMER_H_ */
