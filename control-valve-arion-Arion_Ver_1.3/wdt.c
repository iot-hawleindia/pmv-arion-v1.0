/*
 * wdt.c
 *  Modified on : Feb 04,2020
 *  Created on  : Sep 26, 2020
 *      Author  : Arjun Shankar
 */
#include "config.h"

void wdt_init(void)
{
    /*Hold watch dog timer*/
    stopWdt();

    /* Given Clock source and Time interval (8seconds) to reset */
    WDT_A_initWatchdogTimer ( WDT_A_BASE, WDT_A_CLOCKSOURCE_SMCLK,WDT_RESET_8SEC);

    /* Start the watch dog timer */
    WDT_A_start(WDT_A_BASE);

}

void turnOnXt1Clock ( void )
{
    /* Select XT1 clock pin */
    GPIO_setAsPeripheralModuleFunctionInputPin(PORT_XT1, PIN_XT1_IN + PIN_XT1_OUT);

    /* Initialize XT1 in LF mode */
    UCS_turnOnLFXT1(UCS_XT1_DRIVE_0, UCS_XCAP_0); // changed for RTC drift issue UCS_XCAP_0 - no capacitor connected internally in MC
}

void notifyWDT(void)
{
    WDT_A_resetTimer(WDT_A_BASE);
}

void stopWdt ( void )
{
    /*hold watchdog timer*/
    WDT_A_hold(WDT_A_BASE);
}
