/*
 * wdt.h
 *  Modified on : Feb 05,2020
 *  Created on  : Sep 26, 2020
 *      Author  : Arjun Shankar
 */

#ifndef WDT_H_
#define WDT_H_

#define WDT_RESET_8SEC     WDTIS_2

/*-----------------------------------------------------------------
NAME        : void wdt_init(void);
PROCESS     : Initialize Watchdog timer for the application
-----------------------------------------------------------------*/
void wdt_init(void);
/*-----------------------------------------------------------------
NAME        : void stopWdt ( void );
PROCESS     : Hold Watchdog timer
-----------------------------------------------------------------*/
void stopWdt ( void );
/*-----------------------------------------------------------------
NAME        : void turnOnXt1Clock ( void );
PROCESS     : Configure XT1 pins for external clock
-----------------------------------------------------------------*/
void turnOnXt1Clock ( void );
/*-----------------------------------------------------------------
NAME        : void notifyWDT(void);
PROCESS     : Reset watchdog timer
-----------------------------------------------------------------*/
void notifyWDT(void);

#endif /* WDT_H_ */


